@extends('layouts.apps')
<link rel="stylesheet" href="{{asset('css/layout.css') }}">


@section('header')
    @include('admin.header')
@endsection
@section('content')
    <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
    <div  class="container col-md-offset-1" >
        <button type="button" class="button" id="add_company1" onclick="location.href='/admin/promocode';">Add Promocode</button>
        <button type="button" class="button" id="add_company2" onclick="location.href='/admin/promousers';">Promocode Users</button>
    <div class="modal-content">
        <div class="modal-header">

            <h4 class="modal-title">Edit Promocode</h4>
        </div>
        <div class="modal-body">
            <form method="post" action="" id="editpromocode">
                <input type="hidden" id="id" name="id" value="{{$promo[0]->id}}">
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="Name">Name: <span class="required">*</span></label>
                        <input type="text" class="form-control" name="Name" id="Name" value="{{$promo[0]->name}}" required>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="lamount">Promo Code: <span class="required">*</span></label>
                        <input type="text"  class="form-control" name="code" id="code" value="{{$promo[0]->promocode}}" required>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="discount">Discount: <span class="required">*</span></label>

                        <input type="number" min="0" max="100" class="form-control" name="discount" id="discount" value="{{$promo[0]->discount}}" required>
                    </div>
                </div>
                <div class="col-md-12 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="active">Status: <span class="required">*</span></label>
                        <select name="active" id="active" class="form-control">
                            <option value="active">Active</option>
                            <option value="deactive">Deactive</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <input type="submit" name="submit" value="Edit"  id="editpromoformbtn"  class="btn btn-primary " style="background:#01A156;height:50px; margin-bottom:15px;width:37%">
                    </div>
                </div>


                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div id="editpromoformmsg"></div>
                </div>
            </form>
            <div style="clear:both;"></div>
        </div>

    </div>

    {{--</div>--}}
    {{--</div>--}}





@endsection
@section('footer')
    @include('admin.footer')
@endsection
<script src="https://code.jquery.com/jquery-3.1.1.js" ></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function() {
        $('#editpromoformbtn').click(function () {
            var crsfToken = $("#_token").val();
            var id=$("#id").val();
            var Name = $("#Name").val();
            var code = $("#code").val();
            var discount = $("#discount").val();
            var active = $("#active").val();
            $.ajax({
                url: "{{url('/admin/editpromocode')}}", // use your target
                type: "POST",
                data: "id="+id+"&Name=" + Name + "&code=" + code + "&discount=" + discount + "&active=" + active,

                headers: {
                    "X-CSRF-TOKEN": crsfToken
                },
                success: function (data) {

                    $("#editpromoformbtn").html(data).fadeIn(1000).fadeOut(10000);
                    location.reload();

                }
            });
            return false;
        });




    });

</script>




