@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row" style="text-align:center; padding:15px 0px; border-bottom:2px solid #900">
                        <div class="col-md-4 col-sm-6"  >
                            <img src="{{ asset('img/Neurons-inc-consultancy.png') }}" width="200" >
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <h4 style="text-align:center;padding-top:15px;">Reset Password</h4>
                        </div>
                        <div class="col-md-4 col-xs-6">
                            <img src="{{ asset('img/NeuroVision.png') }}" width="85" style="" />
                        </div>
                    </div></div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
