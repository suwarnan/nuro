@extends('layouts.apps')
@section('header')
    @include('admin.header')
@endsection
@section('content')
    <div class="container">
        <div class="row-fluid">
            <div id="content">

                <div class="col-sm-12" style="background:#CCC;margin:10px 0px">
                    <span style="font-size:19px;color:#999">Edit Users </span>
                </div>


                <form method="post" action="{{url('/admin/edituser')}}">

                    <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" id="id" name="id" value="{{$my[0]->id}}">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <h5>First Name: </h5>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="first_name" value="{{$my[0]->firstname}}" class="form-control"id="ufirstname">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h5>Last Name: </h5>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text"  name="last_name"  value="{{$my[0]->lastname}}"       class="form-control"id="ulastname">
                                </div>
                            </div>



                            <div class="col-md-4">
                                <h5>Email: </h5>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text"  name="email"  value="{{$my[0]->email}}"        class="form-control"id="ulastname">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <h5>Status: </h5>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" style="width:100%">

                                    <select id="name" name="status"  class="form-control">
                                        <option value="Block">Select </option>
                                        <option value="Block">Block </option>
                                        <option value="Unblock">Unblock</option>
                                    </select>

                                </div>
                            </div>

                            <div class="col-md-6 " >
                                <div class="form-group" >
                                    <input type="submit"  name="submit" value="Update"  >
                                </div>
                            </div>

                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    @endsection
@section('footer')
    @include('admin.footer')
@endsection