@extends('layouts.apps')
@section('header')
    @include('admin.header')
@endsection
@section('content')
    <div class="container" id="Uploadimages">
        <div class="row-fluid">

            <div id="content" >


                <div class="col-sm-6 col-md-6 col-xs-6" style="background:#CCC;margin:10px 0px">
                    <span style="font-size:19px;color:#999">original Image</span>
                </div>
                <div class="col-sm-6 col-md-6 col-xs-6" style="background:#CCC;margin:10px 0px">
                    <span style="font-size:19px;color:#999">Heat Image</span>
                </div>
                <div class="row" style="margin:10px 0px;padding-bottom:10px">
                    @foreach($heatmap as $heat)
                        {{--{{$heat}}--}}
                        @inject('admin', 'App\Http\Controllers\adminController')
                        <div class="col-sm-6 col-md-6 col-xs-6" style="padding:10px 0px">
                            <div class="col-sm-6 col-md-6 col-xs-6" style="text-align:center">
                                <a href="#">
                                    <img width="200"  src="{{asset('image_data/'.$admin->getuserdetails($heat->user_name)[0]->username.'/'.$heat->folder.'/'. $heat->images)}}" class=" " alt=""/>
                                </a>

                            </div>

                            <div class="col-sm-6 col-md-6 col-xs-6">
                                <p class="pull-left"><h4 style="line-height:0.1"></h4>
                                <div><span style="font-size:10px"></span></div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-xs-6" style="padding:10px 0px">
                            <div class="col-sm-6 col-md-6 col-xs-6" style="text-align:center">
                                <a href="#">
                                    <img width="200"  src="{{asset('image_data/'.$admin->getuserdetails($heat->user_name)[0]->username.'/'.$heat->folder.'/unzip_'.$heat->images.'/'.$heat->images.'_heat.jpg')}}" class=" " alt=""/>
                                </a>
                            </div>

                            <div class="col-sm-6 col-md-6 col-xs-6">
                                <p class="pull-left"><h4 style="line-height:0.1"></h4>
                                <div><span style="font-size:10px"> </span></div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>

        </div>

        <div class="clearfix"></div>
    </div>
@endsection
@section('footer')
    @include('admin.footer')
@endsection