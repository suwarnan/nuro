@extends('layouts.apps')
<style>
    #myform {
        text-align: center;
        padding: 5px;
        border: 1px dotted #ccc;
        margin: 2%;
    }
    .qty {
        width: 40px;
        height: 25px;
        text-align: center;
    }
    input.qtyplus { width:25px; height:25px;}
    input.qtyminus { width:25px; height:25px;}</style>
@section('header')
    @include('admin.header')
@endsection
@section('content')

<div class="container">
    <div class="row-fluid">
        <div id="content">

            <div class="col-sm-12" style="background:#CCC;margin:10px 0px">
                <span style="font-size:19px;color:#999">Users </span>
            </div>

            @foreach($usercredit as $usercredits)
            <div class="row" style="border-bottom:1px dashed #CCC;margin:10px 0px;padding-bottom:10px">

                <div class="col-sm-2 col-md-2 col-xs-6">
                    <h4> {{$usercredits->firstname}} {{$usercredits->lastname}} </h4>
                </div>
                <div class="col-sm-2 col-md-2 col-xs-6">
                    <h5>
                        <form id='myform' method='POST' action='#'>
                            <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                            <input type='button' value='-' class='qtyminus btn-danger' field='quantity' onclick="qtyminus('quantity_{{$usercredits->id}}')"/>
                            <input type='text' name='quantity_{{$usercredits->id}}' id='quantity_{{$usercredits->id}}' value='{{$usercredits->total_credit}}' class='qty' />
                            <input type='button' value='+' class='qtyplus btn-success' field='quantity' onclick="qtyplus('quantity_{{$usercredits->id}}')" />
                        </form>

                    </h5>


                </div>
                <div class="col-sm-2 col-md-2 col-xs-6">
                    <h5> <input type='button' value='Save' class='btn btn-success' onclick="save('{{$usercredits->id}}','quantity_{{$usercredits->id}}')" /> </h5>

                </div>

                <div class="col-sm-2 col-md-5 col-xs-6">

                    <h5 class="quantity_msg_{{$usercredits->id}}"> </h5>
                </div>



            </div>
          @endforeach

        </div>
    </div>
</div>

@endsection
@section('footer')
    @include('admin.footer')
@endsection
<script src="https://code.jquery.com/jquery-3.1.1.js" ></script>



<script>

    function qtyplus(fieldName){


        var fieldName = fieldName;
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If is not undefined

        $('input[name='+fieldName+']').val(currentVal + 1);


    }

    function qtyminus(fieldName)
    {

        var fieldName = fieldName;
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('input[name='+fieldName+']').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }

    }

    function save(id , fieldName)
    {
        var id = id ;
        var crsfToken = $( "#_token" ).val();
        var value  = $("#"+fieldName).val();
//         alert(value);
        $.ajax({
            type: 'POST',
            url: "{{url('/admin/updatecredit')}}",
            data: "id="+id+"&value="+value,
            headers: {
                "X-CSRF-TOKEN": crsfToken
            },
            success: function(html){
                // alert("done");
                $(".quantity_msg_"+id).html(html);
            }

        });
        return false;

    }
    </script>