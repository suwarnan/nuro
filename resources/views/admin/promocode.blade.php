@extends('layouts.apps')
<link rel="stylesheet" href="{{asset('css/layout.css') }}">


@section('header')
    @include('admin.header')
@endsection
@section('content')
    <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
<div  class="container col-md-offset-1" >
    <button type="button" class="button" id="add_company1" onclick="location.href='/admin/promocode';">Add Promocode</button>
    <button type="button" class="button" id="add_company2" onclick="location.href='/admin/promousers';">Promocode Users</button>

    <h4>Promocode Add/Delete/Edit</h4>

    <a href="javascript:void(0)" data-toggle="modal" data-target="#AddPromo" > <button type="button" class="button" id="add_company">Add New</button></a>

    <div class="table-responsive">
        <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Code</th>
            <th>Discount</th>
            <th>Active</th>
            <th>Functions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($promocode as $promo)
            <tr>
            <td>{{$promo->id}}</td>
            <td>{{$promo->name}}</td>
            <td>{{$promo->promocode}}</td>
            <td>{{$promo->discount}}</td>
            <td>{{$promo->active}}</td>
            <td>
            <a data-id="{{$promo->id}}" title="Edit this item" class="EditDialog btn btn-primary" href="#EditPromo">Edit</a>
            <!-- <a href="javascript:void(0)" data-toggle="modal" data-target="#EditPromo" > <button type="button" class="button" id="edit_company">Edit</button></a> -->
            <!-- <a href="javascript:void(0)" data-toggle="modal" data-target="#DeletePromo" > <button type="button" class="button" id="delete_company">Delete</button></a></td> -->
            <a data-id="{{$promo->id}}" title="Delete this item" class="DeleteDialog btn btn-danger" >Delete</a>
<!--             <a href="/admin/promocode/{{$promo->id}}/edit">Edit</a>/<a href="/admin/promo/{{$promo->id}}/delete">Delete</a></td>
 -->            </tr>
            @endforeach
        </tbody>
    </table>
        </div>

</div>

    <div id="AddPromo" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Promocode</h4>
                </div>
                <div class="modal-body">
                    <form  method="POST" id="addpromoform">
                        <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="Name">Name: <span class="required">*</span></label>
                                <input type="text" class="form-control" name="Name" id="Name" value="" required>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="code">Promo Code: <span class="required">*</span></label>
                                <input type="text"  class="form-control" name="code" id="code" value="" required>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="discount">Discount: <span class="required">*</span></label>

                                    <input type="number" min="0" max="100" class="form-control" name="discount" id="discount" value="" required>
                                  </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                        <label for="active">Status: <span class="required">*</span></label>
                        <select name="active" id=active class="form-control">
                            <option value="active">Active</option>
                            <option value="deactive">Deactive</option>
                        </select>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="submit" name="submit" value="ADD"  id="addpromoformbtn"  class="btn btn-primary " style="background:#01A156;height:50px; margin-bottom:15px;width:37%">
                            </div>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div id="addpromoformmsg"></div>
                        </div>
                    </form>
                    <div style="clear:both;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div id="EditPromo" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Promocode</h4>
                </div>
                <div class="modal-body">
                    <form  method="POST" id="editpromoform">
                        <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="editName">Name: <span class="required">*</span></label>
                                <input type="text" class="form-control" name="editName" id="editName" value="" required>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="editcode">Promo Code: <span class="required">*</span></label>
                                <input type="text"  class="form-control" name="editcode" id="editcode" value="" required>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="editdiscount">Discount: <span class="required">*</span></label>

                                    <input type="number" min="0" max="100" class="form-control" name="editdiscount" id="editdiscount" value="" required>
                                  </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                        <label for="editactive">Status: <span class="required">*</span></label>
                        <select name="editactive" id=editactive class="form-control">
                            <option value="active">Active</option>
                            <option value="deactive">Deactive</option>
                        </select>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="submit" name="submit" value="Edit"  id="editpromoformbtn"  class="editpromoformbtn btn btn-primary " style="background:#01A156;height:50px; margin-bottom:15px;width:37%">
                            </div>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div id="editpromoformmsg"></div>
                        </div>
                    </form>
                    <div style="clear:both;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


    {




    @endsection
@section('footer')
    @include('admin.footer')
@endsection
<script src="https://code.jquery.com/jquery-3.1.1.js" ></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
var Id=0;
    $(document).ready(function() {
        $('#addpromoformbtn').click(function () {
            var crsfToken = $("#_token").val();
            var Name = $("#Name").val();
            var code = $("#code").val();
            var discount = $("#discount").val();
            var active = $("#active").val();
            $.ajax({
                url: "{{url('/admin/addpromocode')}}", // use your target
                type: "POST",
                data: "Name=" + Name + "&code=" + code + "&discount=" + discount + "&active=" + active,

                headers: {
                    "X-CSRF-TOKEN": crsfToken
                },
                success: function (data) {

                    $("#addpromoformmsg").html(data).fadeIn(1000).fadeOut(10000);
                    location.reload();

                }
            });
            return false;
        });

        $(document).on("click", ".EditDialog", function (e) {
            var crsfToken = $("#_token").val();
            e.preventDefault();

            var _self = $(this);

             Id = _self.data('id');
            $.ajax({
                
                url: "{{url('/admin/geteditpromo')}}", // use your target
                type: "POST",
                data: "id="+Id,
                headers: {
                    "X-CSRF-TOKEN": crsfToken
                },
                success: function (data) {

                    // alert(data[0].id);
                    $('#editName').val(data[0].name);
                    $('#editcode').val(data[0].promocode);
                    $('#editdiscount').val(data[0].discount);
                $(_self.attr('href')).modal('show');
            }
            });
            // $("#bookId").val(myBookId);

            
        });

        $(document).on("click", ".editpromoformbtn", function (e) {
            var crsfToken = $("#_token").val();
            Name=$('#editName').val();
            code=$('#editcode').val();
            discount=$('#editdiscount').val();
            Status=$('#editactive').val();
            e.preventDefault();
            $.ajax({
                
                url: "{{url('/admin/editpromocode')}}", // use your target
                type: "POST",
                data: "id="+Id+"&Name="+Name+"&code="+code+"&discount="+discount+"&active="+Status,
                headers: {
                    "X-CSRF-TOKEN": crsfToken
                },
                success: function (data) {
                    $("#editpromoformmsg").html(data).fadeIn(1000).fadeOut(10000);
                    location.reload();

            }
            });
        });

        $(document).on("click", ".DeleteDialog", function (e) {
                var _self = $(this);
                delete_id = _self.data('id');
                swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {
                    // alert(delete_id);
                    window.location="/admin/promo/"+delete_id+"/delete";
            });

        });






    });

</script>




