@extends('layouts.apps')
@section('header')
    @include('admin.header')
@endsection
@section('content')
    <div class="container" id="Uploadimages">
        <div class="row-fluid">

            <div id="content" >


                <div class="col-sm-12" style="background:#CCC;margin:10px 0px">
                    <span style="font-size:19px;color:#999">Images </span>
                </div>

                <div class="row" style="margin:10px 0px;padding-bottom:10px">
                   @foreach($image as $images)
                    <div class="col-sm-6 col-md-4 col-xs-12" style="padding:10px 0px">
                        @inject('admin', 'App\Http\Controllers\adminController')
                        <div class="col-sm-6 col-md-6 col-xs-6" style="text-align:center">
                            <a href="#">
                                <img width="170"  src="{{asset('image_data/'.$admin->getuserdetails($images->user_name)[0]->username.'/'.$images->folder.'/'. $images->images)}}" class=" " alt=""/>
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-6 col-xs-6">
                            <p class="pull-right"><h4 style="line-height:0.1"></h4>
                            <div><span style="font-size:10px"></span></div>
                            <form method="post" action="heatmap">
                                <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="id" value="{{$images->id}}" >
                                <button class="btn btn-warning" style="margin-top:10px">Heat Map</button>
                            </form>
                            <form method="post" action="fogmap">
                                <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="id" value="{{$images->id}}" >
                                <button class="btn btn-danger"style="margin-top:10px">Fog Map</button>
                            </form>
                            </p>
                        </div>
                    </div>
                    @endforeach
                </div>

            </div>

        </div>

        <div class="clearfix"></div>
    </div>
@endsection
@section('footer')
    @include('admin.footer')
@endsection