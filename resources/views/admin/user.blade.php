@extends('layouts.apps')
@section('header')
    @include('admin.header')
@endsection

    <style>
* {
  box-sizing: border-box;
}

#myInput {
  background-image: url('../img/search.png');
  background-position: 10px 10px;
  background-repeat: no-repeat;
  margin-left: 70%;
  width: 30%;
  font-size: 16px;
  padding: 6px 10px 6px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

#myTable {
  border-collapse: collapse;
  width: 100%;
  border: 0px solid #ddd;
  font-size: 18px;
}

#myTable th, #myTable td {
  text-align: left;
  padding: 12px;
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #f1f1f1;
}
</style>
@section('content')
<div class="container">
    <div class="row-fluid">
        <div id="content">

            <div class="col-sm-12" style="background:#CCC;margin:10px 0px">
                <span style="font-size:19px;color:#999">Users </span>
            </div>
            <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
            <table id="myTable">
             @foreach($user as $users)
            <tr>
                <td>{{$users->firstname}} {{$users->lastname}}</td>
                <td>{{$users->status}}</td>
                <td><a href="/admin/user/{{$users->id}}/edit">Edit</a></td>
            </tr>
            @endforeach
            </table>
          <!--  @foreach($user as $users)

            <div class="row" style="border-bottom:1px dashed #CCC;margin:10px 0px;padding-bottom:10px">

                <div class="col-sm-2 col-md-2 col-xs-6">
                    <h4> {{$users->firstname}} {{$users->lastname}} </h4>
                </div>
                <div class="col-sm-2 col-md-2 col-xs-6">
                    <h5>  </h5>
                </div>
                <div class="col-sm-2 col-md-2 col-xs-6">
                    <h5>{{$users->status}}</h5>

                </div>

                <div class="col-sm-2 col-md-2 col-xs-6">
                    <a href="/admin/user/{{$users->id}}/edit">Edit</a>
                    {{--/<a href="/admin/user/delete/{{$users->id}}">Delete</a>--}}
                </div>


            </div>
           @endforeach -->

        </div>
    </div>
</div>

@endsection
<script>
function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>
@section('footer')
    @include('admin.footer')
@endsection