@extends('layouts.apps')
<link rel="stylesheet" href="{{asset('css/layout.css') }}">
@section('header')
    @include('admin.header')
@endsection
@section('content')
    <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
    <div class="container col-md-offset-1">
        <button type="button" class="button" id="add_company1" onclick="location.href='/admin/referalcode';">Add Refferal</button>
        <button type="button" class="button" id="add_company2" onclick="location.href='/admin/refcode';">Refferal</button>
        <button type="button" class="button" id="add_company3" onclick="location.href='/admin/userearnref';">User Earn</button>
        <button type="button" class="button" id="add_company4" onclick="location.href='/admin/ownerearnref';">Owner Earn</button>
        <h4>User Refferal Codes</h4>



        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>User ID</th>
                    <th>User Name</th>
                    <th>Refferal Code</th>
                    <th>Type</th>
                    <th>Function</th>

                </tr>
                </thead>
                <tbody>
                @foreach($refcode as $ref)
                    <tr>
                        <td>{{$ref->user_id}}</td>
                        <td>{{$ref->username}}</td>
                        <td>{{$ref->refcode}}</td>
                        <td>{{$ref->type}}</td>
                        <td><a data-id="{{$ref->user_id}}" title="Edit this item" class="EditDialog btn btn-primary" href="#EditRefType" >Edit</a></td>
                           </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    <div id="EditRefType" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Refferal code</h4>
                </div>
                <div class="modal-body">
                    <form  method="POST" id="editrefform">
                        <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="editactive">Type: <span class="required">*</span></label>
                                <select name="editactive" id=editactive class="form-control">
                                    @foreach($refdis as $refd)
                                    <option value="{{$refd->id}}">{{$refd->Name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="submit" name="submit" value="Edit"  id="editrefformbtn"  class="editrefformbtn btn btn-primary " style="background:#01A156;height:50px; margin-bottom:15px;width:37%">
                            </div>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div id="editrefformmsg"></div>
                        </div>
                    </form>
                    <div style="clear:both;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            </div>
            </div>







@endsection
@section('footer')
    @include('admin.footer')
@endsection
<script src="https://code.jquery.com/jquery-3.1.1.js" ></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function() {
        $Id=0;
        $(document).on("click", ".EditDialog", function (e) {
            var crsfToken = $("#_token").val();
            e.preventDefault();

            var _self = $(this);

             Id = _self.data('id');
            $(_self.attr('href')).modal('show');
            

            
        });

         $(document).on("click", ".editrefformbtn", function (e) {
            var crsfToken = $("#_token").val();
            e.preventDefault();
            Type=$('#editactive').val();
            // alert(Type+" "+Id);

            $.ajax({
                
                url: "{{url('/admin/editreftype')}}", // use your target
                type: "POST",
                data: "id="+Id+"&Type="+Type,
                headers: {
                    "X-CSRF-TOKEN": crsfToken
                },
                success: function (data) {
                    $("#editrefformmsg").html(data).fadeIn(1000).fadeOut(10000);
                    location.reload();

            }
            });
        });
    });
</script>