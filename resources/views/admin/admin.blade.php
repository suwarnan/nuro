@extends('layouts.apps')
@section('header')
@include('admin.header')
@endsection
@section('content')
    <div class="container">
        <div class="row-fluid">
            <div class="col-sm-12" style="background:#CCC;margin:10px 0px">
                <span style="font-size:19px;color:#999">Users </span>
            </div>
            @foreach($user as $users)
            <div class="row" style="border-bottom:1px dashed #CCC;margin:10px 0px;padding-bottom:10px">
                <div class="col-sm-2 col-md-1 col-xs-6">
                    <i class="fa fa-user"></i>
                </div>
                <div class="col-sm-3 col-md-3 col-xs-6">
                    <h4>{{$users->firstname}} {{$users->lastname}}</h4>
                </div>
                <div class="col-sm-3 col-md-3 col-xs-6">
                    <h5> </h5>
                </div>
                <div class="col-sm-2 col-md-3 col-xs-6">
                    <h5>{{$users->status}}</h5>
                </div>
                <div class="col-sm-2 col-md-2 col-xs-6">

                </div>

            </div>@endforeach
                <div class="row" >
                    <div class="col-md-12">
                        <a href="/admin/user"><button class="btn btn-default pull-right">Show all User</button>    </a>
                    </div>
                </div>
        </div>
    </div>
    <!-- video ---------------->
    <!-- <div class="container" id="Uploadvideo">
        <div class="row-fluid">
            <div class="col-sm-12" style="background:#CCC;margin:10px 0px">
                <span style="font-size:19px;color:#999">Uploaded Videos </span>
            </div>
        @foreach($video as $videos)
            <div class="row" style="border-bottom:1px dashed #CCC;margin:10px 0px;padding-bottom:10px">
                @inject('admin', 'App\Http\Controllers\adminController')
                <div class="col-sm-2 col-md-2 col-xs-12">
                    <video width="120" height="100" controls style="border:2px solid #ff2200; text-align:center" preload="auto">
                        <source src="{{asset('video_data/'.$admin->getuserdetails($videos->user_name)[0]->username .'/'.$videos->folder.'/'.$videos->video_name)}}" type="video/mp4">
                    </video>

                </div>

                <div class="col-sm-8 col-md-8 col-xs-9" style="word-wrap: break-word;">
                    <h4>{{$videos->video_name}}</h4>
                    <b> by {{$admin->getuserdetails($videos->user_name)[0]->username}}</b> <br>
                </div>


                <div class="col-sm-2 col-md-2 col-xs-3">

                </div>



            </div>
    @endforeach
            <div class="row" >

                <div class="col-md-12">
                    <a href="/admin/videoupload"><button class="btn btn-default pull-right">Show all Videos</button>    </a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="container"id="Uploadimages">
        <div class="row-fluid">


            <div class="col-sm-12" style="background:#CCC;margin:10px 0px">
                <span style="font-size:19px;color:#999">Uploaded Images</span>
            </div>
            <div class="row" style="margin:10px 0px;padding-bottom:10px">
@foreach($image as $images)


                <div class="col-sm-6 col-md-4 col-xs-6" style="padding:10px 0px">
                    <div class="col-sm-6 col-md-6 col-xs-6" style="text-align:center">
                        @inject('admin', 'App\Http\Controllers\adminController')
                        <a href="#">
                            <img width="170"  src="{{asset('image_data/'.$admin->getuserdetails($images->user_name)[0]->username.'/'.$images->folder.'/'. $images->images)}}" class=" " alt=""/>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-6 col-xs-6">
                        <p class="pull-right">
                            <span style="font-size:10px">by {{$admin->getuserdetails($images->user_name)[0]->username}}</span>
                        </p>
                    </div>
                </div>
    @endforeach
                <div class="row" >
                    <div class="col-md-12">
                        <a href="/admin/imagesupload"><button class="btn btn-default pull-right">Show all Images</button>    </a>
                    </div>
                </div>

            </div>

        </div>
        <div class="clearfix"></div>
    </div> -->

@endsection

@section('footer')
    @include('admin.footer')
@endsection