@extends('layouts.apps')
@section('header')
    @include('admin.header')
@endsection
@section('content')
    <div class="container" id="Uploadvideo">
        <div class="row-fluid">
            <div id="content" >

                <div class="col-sm-12" style="background:#CCC;margin:10px 0px">
                    <span style="font-size:19px;color:#999">Uploaded Videos </span>
                </div>
              @foreach($video as $videos)
                <div class="row" style="border-bottom:1px dashed #CCC;margin:10px 0px;padding-bottom:10px">
                    @inject('admin', 'App\Http\Controllers\adminController')
                    <div class="col-sm-2 col-md-2 col-xs-12">
                        <video width="120" height="100" controls style="border:2px solid #ff2200; text-align:center" preload="metadata">
                            <source src="{{asset('video_data/'.$admin->getuserdetails($videos->user_name)[0]->username .'/'.$videos->folder.'/'.$videos->video_name)}}" type="video/mp4">
                        </video>
                    </div>
                    <div class="col-sm-8 col-md-8 col-xs-9" style="word-wrap: break-word;">
                        <h4>{{$videos->video_name}}</h4>
                        <b> by {{$admin->getuserdetails($videos->user_name)[0]->username}}</b> <br>
                    </div>

                    <div class="col-sm-2 col-md-2 col-xs-3">
                        <form method="post" action="/admin/videoheatmap">
                            <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="id" value="{{$videos->id}}" >
                            <button class="btn btn-warning" style="margin-top:10px">Heat Map</button>
                        </form>

                    </div>

                    <div class="col-sm-2 col-md-2 col-xs-3">


                    </div>
                </div>

@endforeach




            </div>

        </div>
        <div class="clearfix"></div>
    </div>
@endsection
@section('footer')
    @include('admin.footer')
@endsection