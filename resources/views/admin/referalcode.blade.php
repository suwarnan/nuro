@extends('layouts.apps')
<link rel="stylesheet" href="{{asset('css/layout.css') }}">
@section('header')
    @include('admin.header')
@endsection
@section('content')
    <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
    <div class="container col-md-offset-1">
        <button type="button" class="button" id="add_company1" onclick="location.href='/admin/referalcode';">Add Refferal</button>
        <button type="button" class="button" id="add_company2" onclick="location.href='/admin/refcode';">Refferal</button>
        <button type="button" class="button" id="add_company3" onclick="location.href='/admin/userearnref';">User Earn</button>
        <button type="button" class="button" id="add_company4" onclick="location.href='/admin/ownerearnref';">Owner Earn</button>

        <h4>Add/Delete/Edit Refferal Code</h4>

        <a href="javascript:void(0)" data-toggle="modal" data-target="#AddRef" ><button type="button" class="button" id="add_company">Add New</button></a>

        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>User Extra Credit</th>
                    <th>Owner Extra Credit</th>
                    <th>Status</th>
                    <th>Functions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($refcode as $ref)
                    <tr>
                <td>{{$ref->id}}</td>
                <td>{{$ref->Name}}</td>
                <td>{{$ref->user_discount}}</td>
                <td>{{$ref->owner_discount}}</td>
                <td>{{$ref->active}}</td>
                <td>
                <a data-id="{{$ref->id}}" title="Edit this item" class="EditDialog btn btn-primary" href="#EditRef" >Edit</a> 
                {{--/<a href="/admin/ref/{{$ref->id}}/edit">Edit</a>--}}
                    @if($ref->id != '1')
                    <!-- <a href="/admin/ref/{{$ref->id}}/delete">Delete</a></td> -->
                    <a data-id="{{$ref->id}}" title="Delete this item" class="DeleteDialog btn btn-danger" >Delete</a>
                    @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>

    <div id="AddRef" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Refferalcode</h4>
                </div>
                <div class="modal-body">
                    <form  method="POST" id="addrefform">
                        <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="Name">Name: <span class="required">*</span></label>
                                <input type="text" class="form-control" name="Name" id="Name" value="" required>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="userdiscount">User Discount: <span class="required">*</span></label>
                                <input type="text"  class="form-control" name="userdiscount" id="userdiscount" value="" required>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="ownerdiscount">Owner Discount: <span class="required">*</span></label>

                                <input type="number" min="0" max="100" class="form-control" name="ownerdiscount" id="ownerdiscount" value="" required>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="active">Status: <span class="required">*</span></label>
                                <select name="active" id=active class="form-control">
                                    <option value="active">Active</option>
                                    <option value="deactive">Deactive</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="submit" name="submit" value="ADD"  id="addrefformbtn"  class="btn btn-primary " style="background:#01A156;height:50px; margin-bottom:15px;width:37%">
                            </div>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div id="addrefformmsg"></div>
                        </div>
                    </form>
                    <div style="clear:both;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div id="EditRef" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Refferal code</h4>
                </div>
                <div class="modal-body">
                    <form  method="POST" id="editrefform">
                        <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="editName">Name: <span class="required">*</span></label>
                                <input type="text" class="form-control" name="editName" id="editName" value="" required>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="edituserdiscount">User Discount: <span class="required">*</span></label>
                                <input type="text"  class="form-control" name="edituserdiscount" id="edituserdiscount" value="" required>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="editownerdiscount">Owner Discount: <span class="required">*</span></label>

                                <input type="number" min="0" max="100" class="form-control" name="editownerdiscount" id="editownerdiscount" value="" required>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="editactive">Status: <span class="required">*</span></label>
                                <select name="editactive" id=editactive class="form-control">
                                    <option value="active">Active</option>
                                    <option value="deactive">Deactive</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="submit" name="submit" value="Edit"  id="editrefformbtn"  class="editrefformbtn btn btn-primary " style="background:#01A156;height:50px; margin-bottom:15px;width:37%">
                            </div>
                        </div>


                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div id="editrefformmsg"></div>
                        </div>
                    </form>
                    <div style="clear:both;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>






@endsection
@section('footer')
    @include('admin.footer')
@endsection
<script src="https://code.jquery.com/jquery-3.1.1.js" ></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(document).ready(function() {
        $Id=0;
        $('#addrefformbtn').click(function () {
            var crsfToken = $("#_token").val();
            var Name = $("#Name").val();
            var userdiscount = $("#userdiscount").val();
            var ownerdiscount = $("#ownerdiscount").val();
            var active = $("#active").val();
            $.ajax({
                url: "{{url('/admin/addrefcode')}}", // use your target
                type: "POST",
                data: "Name=" + Name + "&userdiscount=" + userdiscount + "&ownerdiscount=" + ownerdiscount + "&active=" + active,

                headers: {
                    "X-CSRF-TOKEN": crsfToken
                },
                success: function (data) {

                    $("#addrefformmsg").html(data).fadeIn(1000).fadeOut(10000);
                    location.reload();
                }
            });
            return false;
        });
         $(document).on("click", ".EditDialog", function (e) {
            var crsfToken = $("#_token").val();
            e.preventDefault();

            var _self = $(this);

             Id = _self.data('id');
            $.ajax({
                
                url: "{{url('/admin/geteditref')}}", // use your target
                type: "POST",
                data: "id="+Id,
                headers: {
                    "X-CSRF-TOKEN": crsfToken
                },
                success: function (data) {

                    // alert(data[0].id);
                    $('#editName').val(data[0].Name);
                    $('#edituserdiscount').val(data[0].user_discount);
                    $('#editownerdiscount').val(data[0].owner_discount);
                $(_self.attr('href')).modal('show');
            }
            });
            

            
        });

        $(document).on("click", ".editrefformbtn", function (e) {
            var crsfToken = $("#_token").val();
            Name=$('#editName').val();
            userdiscount= $('#edituserdiscount').val();
            ownerdiscount=$('#editownerdiscount').val();
            Status=$('#editactive').val();
            
            e.preventDefault();
            $.ajax({
                
                url: "{{url('/admin/editreferaldetails')}}", // use your target
                type: "POST",
                data: "id="+Id+"&Name="+Name+"&userdiscount="+userdiscount+"&ownerdiscount="+ownerdiscount+"&active="+Status,
                headers: {
                    "X-CSRF-TOKEN": crsfToken
                },
                success: function (data) {
                    $("#editrefformmsg").html(data).fadeIn(1000).fadeOut(10000);
                    location.reload();

            }
            });
        });

         $(document).on("click", ".DeleteDialog", function (e) {
                var _self = $(this);
                delete_id = _self.data('id');
                swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {
                    // alert(delete_id);
                    window.location="/admin/ref/"+delete_id+"/delete";
            });

        });


    });
</script>





