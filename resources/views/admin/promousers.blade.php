@extends('layouts.apps')
<link rel="stylesheet" href="{{asset('css/layout.css') }}">


@section('header')
    @include('admin.header')
@endsection
@section('content')
    <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
    <div  class="container col-md-offset-1" >
        <button type="button" class="button" id="add_company1" onclick="location.href='/admin/promocode';">Add Promocode</button>
        <button type="button" class="button" id="add_company2" onclick="location.href='/admin/promousers';">Promocode Users</button>

        <h4>Promocode User Details</h4>


        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                <tr>
                    <th>User Id</th>
                    <th> User Id</th>
                    <th>Date</th>
                    <th>Total Token</th>
                    <th>Discount Token</th>

                </tr>
                </tr>
                </thead>
                <tbody>
                @foreach($promousers as $promo)
                    <tr>
                        <td>{{$promo->id}}</td>
                        <td>{{$promo->user_ID}}</td>
                        <td>{{$promo->Date}}</td>
                        <td>{{$promo->token}}</td>
                        <td>{{$promo->promo_token}}</td>
                        {{--<td><a href="javascript:void(0)" data-toggle="modal" data-target="#EditPromo" >Edit</a>/<a href="/admin/promo/delete/{{$promo->id}}">Delete</a></td>--}}
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>





@endsection
@section('footer')
    @include('admin.footer')
@endsection
<script src="https://code.jquery.com/jquery-3.1.1.js" ></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
