
<div class="container">
    <div class="row-fluid">

        <ul class="breadcrumb">
            <li>
                <i class="fa fa-dashboard fa-3x" style="color:#999"></i>
                <a href="/admin/profile"><span style="font-size:24px;color:#999;">Dashboard </span> </a>
            </li>
        </ul>
        <hr style="border:1px solid #666;">

        <div class="row">
            <div class="col-sm-2 col-md-2">
                <a href="/admin/user">
                    <div class="jumbotron" style="background:#2D89EF; text-align:center">
                        <i class="fa fa-user fa-3x" style="color:#fff"></i>
                        <h4 style="color:#fff">Registered</br>Users</h4>
                    </div>               		</a>
            </div>

            <div class="col-sm-2 col-md-2">
                <a href="/admin/videoupload">
                    <div class="jumbotron" style="background:#00A300;text-align:center">
                        <i class="fa fa-play-circle-o fa-3x" style="color:#fff">
							<span class="counter" style="font-size: 30px">{{count($video)}} </span>
                        </i>
                        <h4 style="color:#fff"> Uploaded video </h4>
                    </div>
                </a>
            </div>
            <div class="col-sm-2 col-md-2">
                <a href="/admin/imagesupload">
                    <div class="jumbotron" style="background:#603CBA;text-align:center">
                        <i class="fa fa-picture-o  fa-3x" style="color:#fff;">
						<span class="counter" style="font-size: 30px">{{count($image)}} </span>
                        </i>
                        <h4 style="color:#fff">Uploaded Picture</h4>
                    </div>
                </a>
            </div>
            <div class="col-sm-2 col-md-2">
                <a href="/admin/managecredit">
                    <div class="jumbotron" style="background:#FF5E00;text-align:center">
                        <i class="fa fa-credit-card  fa-3x" style="color:#fff;"></i>
                        <h4 style="color:#fff">Manage</br> credit</h4>
                    </div>
                </a>
            </div>
            <div class="col-sm-2 col-md-2">
                <a href="/admin/promocode"><div class="jumbotron" style="background:#00A300; text-align:center">
                        <i class="fa fa-user  fa-3x" style="color:#fff"></i>
                        <h4 style="color:#fff">Promo Code</h4>
                    </div>               		</a>
            </div>
            <div class="col-sm-2 col-md-2">
                <a href="/admin/referalcode"><div class="jumbotron" style="background:#2D89EF; text-align:center">
                        <i class="fa fa-user  fa-3x" style="color:#fff"></i>
                        <h4 style="color:#fff">Refferal Code</h4>
                    </div>               		</a>
            </div>
        </div>

    </div>
    <hr style="border:1px solid #666;">
    <div class="clearfix"></div>
</div>

