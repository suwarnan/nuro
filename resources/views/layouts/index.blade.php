<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="icon" href="{{  asset('img/Neurons-inc-consultancy.png') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
     <link href="{{ asset('css/my.css') }}" rel="stylesheet">
    {!! HTML::style('/packages/bootstrap/css/bootstrap.min.css') !!}
    <link rel="stylesheet" href="//cdn.jsdelivr.net/sweetalert2/6.4.4/sweetalert2.min.css">
    <style>
        #loading{
            position: fixed;
            top: 50%;
            left: 50%;
            margin-left: -50px; /* half width of the spinner gif */
            margin-top: -50px; /* half height of the spinner gif */
            text-align:center;
            z-index:1234;
            overflow: auto;
            width: 100px; /* width of the spinner gif */
            height: auto; /*hight of the spinner gif +2px to fix IE8 issue */
        }

        .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {
            background-color:#0099FF;
            
            }
           
.navbar-default .navbar-nav > li > a:hover:after {
    color:#FFF;
    font-size: 12px;
    background:#000;
    background:rgba(0,0,0,.8);
    content:attr(data-title);
    padding:3px 8px;
    white-space:nowrap;
    display:block;
    position:absolute;
    top: 100%;
  bottom: auto;
    left:30%;
    /*bottom:18px;*/
    z-index:8
}

.navbar-default .navbar-nav > li > a:hover:before {
        border:solid;
        border-color:rgba(0,0,0,.8) transparent;
        border-width:6px 6px 0 6px;
        content:"";
        display:block;
        position:absolute;
    left:50%;
    bottom:12px;
        z-index:9
}
.navbar .nav-stacked > li a:hover:after {
    color:#FFF;
    font-size: 12px;
    background:#000;
    background:rgba(0,0,0,.8);
    content:attr(data-title);
    padding:3px 8px;
    white-space:nowrap;
    display:block;
    position:absolute;
    
    bottom: 50%;
    left:100%;
    /*bottom:18px;*/
    z-index:8;
    margin-left: 0;
  margin-bottom: -16px;
}

.navbar .nav-stacked > li a:hover:before {
        border:solid;
        border-color:rgba(0,0,0,.8) transparent;
        border-width:6px 6px 0 6px;
        content:"";
        display:block;
        position:absolute;
        left:50%;
        bottom:12px;
        z-index:9
}
.navbar .nav-stacked > li a:hover,.navbar .nav-stacked > li a:focus{
     background-color:#0099FF;
}


    </style>
    {!! HTML::script('//code.jquery.com/jquery-3.1.1.js') !!}

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
  

</head>
<body >
<!-- <div id="loading">
    <p><img src="{{asset('img/image_923487.gif')}}" /> Please Wait</p>
</div> -->
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">


                        <div class="container-fluid">
                            <div class="row" style="border-bottom:1px solid ">
                                <div class="col-md-4 col-xs-6">
                                    <a href="{{url('/home')}}"><img src="{{  asset('img/NeuroVision.png') }}" width="50" style="padding-top:5px" /></a>
                                </div>
                                <div class="col-md-3 col-xs-6">
                                    <a href="{{url('/home')}}"><img src="{{  asset('img/Neurons-inc-consultancy.png') }}" width="120" style="padding-top:10px" /></a>
                                </div>
                                <div class="col-md-5 col-xs-8">
                                    <ul class="nav navbar-nav  pull-right">
                                        <li>

                                            <a data-title="Total Credits">

                                                <img src="{{  asset('img/1457522957_08.Credit-Card.png')}}" width="40" height="40" data-toggle="tooltip" title="Total Credits" data-placement="bottom"/>

                                                 {{ $credit[0]->total_credit}}
                                                <input type="hidden" id="total_credit" value="{{ $credit[0]->total_credit}} " >
                                            </a>

                                        </li>
                                        <li>
                                            <a  data-toggle="modal" data-target="#myModal" data-title="Add Credits">
                                                <img src="{{  asset('img/1457522984_plus-24.png')}}" width="40" height="40" data-toggle="tooltip" title="Add Credits" data-placement="bottom"/>
                                            </a>
                                        </li>
                                        <li>
                                            <a  data-toggle="modal" data-target="#ContactSupport" data-title="Contact Support" ><img src="{{  asset('img/1457523017_online_support.png') }}" width="40" height="40"  data-toggle="tooltip" title="Contact Support" data-placement="bottom"/></a>

                                        </li>

                                        <li>
                                            <a  data-toggle="modal" data-target="#EnterpriseSolutions" data-title="Enterprise Solutions" ><img src="{{  asset('img/1457523179_solutions.png') }}" width="40" height="40" data-toggle="tooltip" title="Enterprise Solutions" data-placement="bottom"/></a>
                                        </li>
                                        <li>
                                            <a  data-toggle="modal" data-target="#mail" data-title="Share Refferal Code" ><img src="{{  asset('img/share.png') }}" width="40" height="40" title="Share Refferal Code" data-placement="bottom"/></a>
                                        </li>

                                                <li>
                                                    <a href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" data-title="Logout">
                                                        <img src="{{  asset('img/1457523243_logout.png') }}" width="40" height="40" data-toggle="tooltip" title="Logout" data-placement="bottom"/>
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </li>






                                    </ul>
                                </div>
                            </div>
                        </div>


                </ul>


    </nav>

    @yield('content')
    
</div>
<style>
    input.red
    {
        border:2px solid red;
    }
    input.green
    {
        border:2px solid green;
    }
</style>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="//code.jquery.com/jquery-3.1.1.js" ></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.1.0/js/bootstrap.min.js"></script>
<input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Payment</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{url('getCheckout')}}">
                    {{--action="https://www.sandbox.paypal.com/cgi-bin/webscr"--}}
                    <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="item_name" class="form-control" id="groupname" value="credit" readonly="readonly"  >
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" min="1" name="quantity" onkeyup="myFunction()" class="form-control" id="product_quantity" placeholder="product quantity"  required>
                        </div>
                    </div>
                    <input type="hidden" name="discount_amount2" id="discount_amount" value="0">

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="hidden" name="rm" value="2" />
                        <!-- <input type="hidden" name="business" value="Mike@Neurons.com" /> -->
                            <input type="hidden" name="business" value="suwarnan@skan.com" />
                            <input type="hidden" name="lc" value="" />
                            <input type="hidden" name="page_style" value="paypal" />
                            <input name="custom" value="{{ Auth::user()->id }}" id="custom" type="hidden">

                            <input type="hidden" value="_xclick" name="cmd"/>
                            <input type="hidden" name="no_shipping" value="1" />
                            <input type="hidden" name="no_note" value="1" />
                            <input type="hidden" name="currency_code" value="USD" />
                            <input type="hidden" name="notify_url" value="{{url('/ipn')}}" />
                            <input type="hidden" name="cancel_return" value="{{ view('home')}}" />
                            <input type="hidden" name="return" value="{{ view('home')}}" />
                            <input type="text" name="amount1" class="form-control" id="product_amount" placeholder="product amount" readonly="readonly" />
                            <input type="hidden" name="amount" class="form-control" id="amount" >
                            <!-- <input type="hidden"  name="quantity" value='1' > -->
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="total amount" class="form-control" id="total_amount" placeholder="product quantity"  readonly="readonly" />
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" class="form-control" id="discount" placeholder="discount"  readonly="readonly" />
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for=""><input type="checkbox" name="procheck" id="procheck" />
                                Insert Promo code</label>
                            <input type="text" style="display:none;"  onkeyup="checkpromo(this.value);" class="form-control" id="procode" placeholder="Promo code" />
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <input type="submit" name="submit" id="submit" value="Payment" onsubmit="submit()" class="btn btn-primary " style="background:#01A156;height:50px; margin-bottom:15px;width:37%">


                    </div>
                </form>
                <div style="clear:both;"></div>
            </div>
            <div class="modal-footer">
                @if($refdetails!="" && $refdiscount[0]->active=="active")
                    <div id='parent'><p class='notice' >Congratulations!!!  You get {{  $refdiscount[0]->user_discount }} credits for your first payment and your refferal code owner( {{$refowner[0]->username  }} ) gets {{  $refdiscount[0]->owner_discount}} credits</p></div>
                @endif
                @if($refdetails!="" && $refdiscount[0]->active=="deactive")
                    <div id='parent1'><p class='notice' >Your bad luck admin close refcode credit</p></div> ;
                 @endif
                <button type="button" class="btn btn-info pull-left" data-toggle="modal" data-target="#coststructure">View Cost Structure</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<!----------------   End Paypal Form          ------------------ -->

<!----------------   coststructure            ------------------- -->
<div id="coststructure" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Cost Structure</h4>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    <li class="list-group-item list-group-item-success"> 1-100    &nbsp; 8 USD per credit</li>
                    <li class="list-group-item list-group-item-success"> 101-300  &nbsp; 7 USD per credit</li>
                    <li class="list-group-item list-group-item-success"> 301-400  &nbsp; 6 USD per credit</li>
                    <li class="list-group-item list-group-item-success"> 401+     &nbsp; 4 USD per credit</li>
                </ul>
                <div style="clear:both;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<!----------------  End  coststructure            --------------------->
<!----------------    Contact Support         -------------------->

<div id="ContactSupport" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Contact Support</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="" id="contactsupportform">

                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="email" name="email" id="email1" class="form-control" placeholder="Enter Your Email" required >
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="subject" id="subject1" class="form-control" placeholder="Enter Your Subject" required>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <textarea rows="4" cols="50" name="message" id="message1" class="form-control" placeholder="Enter Your Message" required></textarea>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                        <input type="submit" name="submit" value="Send"  id="contactsupportformbtn"  class="btn btn-primary " style="background:#01A156;height:50px; margin-bottom:15px;width:37%">
                        </div>
                    </div>


                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="contactsupportformmsg"></div>
                    </div>
                </form>
                <div style="clear:both;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>





<!----------------  End  Contact Support          -------------------->



<!----------------    Enterprise Solutions         -------------------->

<div id="EnterpriseSolutions" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enterprise Solutions </h4>
            </div>
            <div class="modal-body">
                <form method="post" action="" id="enterprisesolutionsform">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="email" name="email" id="email2" class="form-control" placeholder="Enter Your Email" required >
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="subject" id="subject2" class="form-control" placeholder="Enter Your Subject" required>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <textarea rows="4" cols="50" name="message" id="message2" class="form-control" placeholder="Enter Your Message" required></textarea>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <input type="submit" name="submit"  value="Send" id="enterprisesolutionsbtn" class="btn btn-primary " style="background:#01A156;height:50px; margin-bottom:15px;width:37%">


                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="enterprisesolutionsformmsg"></div>
                    </div>
                </form>
                <div style="clear:both;"></div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<div id="mail" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">SHARE YOUR REFFERAL CODE TO FRIEND</h4>
            </div>
            <div class="modal-body">
                <div class="col-sm-12" style="margin-top:10px;">
                    <form action="" method="post" id="refshare">
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="text" class="form-control" name="email" id="email3" placeholder="Email" required/>
                        </div>

                        <div class="form-group">
                            <label for="content">Content:</label>
                            <textarea type="text" name="content" class="form-control"  id="content" placeholder=" My Refferal Code is {{$refcode[0]->refcode}}   " readonly> My Refferal Code is : {{$refcode[0]->refcode}}</textarea>
                        </div>

                        <div class="col-md-12">
                            <button class="btn btn-primary " style="background:#06F; margin-bottom:15px;width:37%; cursor: pointer;">Reset</button>
                            <button type="submit" name="sub" id="sub" value="{{Auth::user()->email}}" class="btn btn-primary pull-right" style="background:#06F; margin-bottom:15px;width:37%">Send</button>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div id="refshareformmsg"></div>
                        </div>
                    </form>
                </div>
                <div style="clear:both;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<!----------------  End Enterprise Solutions          -------------------->
<script src="//code.jquery.com/jquery-3.1.1.js" ></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.jsdelivr.net/sweetalert2/6.4.4/sweetalert2.min.js"></script>
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.1.0/js/bootstrap.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
  
<script>
$('#loading').hide();
$('#loading').ajaxStart(function() {
$(this).show();
}).ajaxComplete(function() {
$(this).hide();
});
</script>
<script>
 
    $('#procheck').click(function() {
        if($(this).is(':checked')){
            $("#procode").show();
            $("#procode").prop('required',true);
        }
        else{
            $('#procode').val('');
            $("#procode").addClass("red");
            $("#procode").removeClass("green");
            myFunction();
            $("#procode").hide();
            $("#procode").prop('required',false);
        }

    });
    $("#product_quantity").change(function(){

        var product_quantity = $("#product_quantity").val();
        var discount = $("#discount").val();

        if(product_quantity >= 1 && product_quantity <= 100)
        {
            discount = 0 ;
        }
        else
        {
            discount = $("#product_quantity").val()*8 - $("#total_amount").val().slice(0, -1);
        }

        $("#discount").val(discount+'$');

        diss=discount/($("#product_quantity").val()*8) *100;

        $("#procode").val('');
        $('input:checkbox[name=procheck]').attr('checked',false);
        $("#procode").addClass("red");
        $("#procode").removeClass("green");
//        $("#procode").hidden();
    });
    function checkpromo(promocode)
    {
        var crsfToken = $( "#_token" ).val();
//        alert(crsfToken);
        $.ajax({
            url : '{{url("/checkpromocode")}}', // use your target
            type : "POST",
            data : "promocode="+promocode,
            headers: {
                "X-CSRF-TOKEN": crsfToken
            },
            success : function(data) {
//                console.log(data);
                if(data == 0)
                {
                    ab=myFunction();
                    abc= (8-ab)*$("#product_quantity").val();
                   $( "#discount" ).val(parseInt(abc)+"$");
                    diss=$( "#discount" ).val().replace('$', '');
                    $("#procode").addClass("red");
                    $("#procode").removeClass("green");
                     
                }
                else
                {
                    swal({
                        title: 'Successfully inserted Promocode',
                        text: 'Congratulations you used promocode with '+ data +'% discount',
                        type: 'success',
                        confirmButtonText: 'OK'
                    });
//                    alert("Congratulations you used promocode with "+ data +"% discount");
                    diss1=parseInt($( "#discount" ).val().replace('$', ''));

                    di=(100-data) * myFunction()* $("#product_quantity").val()/100 ;
                    
                 
                    $('#discount_amount').val(data);
                    ab=myFunction()*(100-data)/100;
                    $('#total_amount').val(di+"$");
                   abc= (8-ab)*$("#product_quantity").val();
                   $( "#discount" ).val(parseInt(abc)+"$");
                    $('#amount').val(ab);

                    $("#procode").addClass("green");
                    $("#procode").removeClass("red");
                   

                }

            }
        });
        return false;
    }





        $('#contactsupportform').submit( function(e) {
//        e.preventDefault();
        var email = $( "#email1" ).val();
        var subject=$( "#subject1" ).val();
        var message=$( "#message1" ).val()
        var crsfToken = $( "#_token" ).val();
//        alert(crsfToken);
        $.ajax({
            type: 'POST',
            url: '{{url("/contactsupport")}}',
            data: 'email='+email+'&subject='+subject+'&message='+message,
            headers: {
                "X-CSRF-TOKEN": crsfToken
            },
            success: function(data)
            {
                swal({
                    title: 'Success!',
                    text: 'Email Send Successfully',
                    type: 'success',
                    confirmButtonText: 'OK'
                });
//                alert(data);
                $("#contactsupportformmsg").html(data).fadeIn(1000).fadeOut(10000);


            }
        });
        return false;

    });

     $('#enterprisesolutionsform').submit( function(e) {
        var email = $( "#email2" ).val();
        var subject=$( "#subject2" ).val();
        var message=$( "#message2" ).val();
        var crsfToken = $( "#_token" ).val();
        $.ajax({
            type: 'POST',
            url: '{{url("/enterprisesolutions")}}',
            data: 'email='+email+'&subject='+subject+'&message='+message,
            headers: {
                "X-CSRF-TOKEN": crsfToken
            },
            success: function(data)
            {
                swal({
                    title: 'Success!',
                    text: 'Email Send Successfully',
                    type: 'success',
                    confirmButtonText: 'OK'
                });
                $("#enterprisesolutionsformmsg").html(data).fadeIn(1000).fadeOut(10000);
            }
        });
        return false;

    });

    $('#refshare').submit( function(e) {
        var email = $( "#email3" ).val();
        var message=$( "#content" ).val();
        var crsfToken = $( "#_token" ).val();
        var sender=$( "#sub" ).val();
        $.ajax({
            type: 'POST',
            url: '{{url("/refshare")}}',
            data: 'email='+email+'&message='+message+'&sender='+sender,
            headers: {
                "X-CSRF-TOKEN": crsfToken
            },
            success: function(data)
            {
                swal({
                    title: 'Success!',
                    text: 'Refferal code share Successfully',
                    type: 'success',
                    confirmButtonText: 'OK'
                });
                $("#refshareformmsg").html(data).fadeIn(1000).fadeOut(10000);
            }
        });
        return false;

    });




    $("#submit").click(function(){
        var product_quantity = $("#product_quantity").val();
        if(product_quantity <=0)
        {

            return false;
        }
        else if($("#procode").hasClass( "red" ) && $('#procheck').is(':checked') ){
            return false;
        }

        else
        {
            return true;
        }

    });
    function myFunction()
    {
        var product_amount ;
        var product_quantity = $("#product_quantity").val();
        // var discount = $("#discount").val();
        if(product_quantity >= 1 && product_quantity <= 100 )
        {
            product_quantity = 8;

        }
        else if(product_quantity >= 101 && product_quantity <= 300)
        {
            product_quantity = 7;



        }
        else if(product_quantity >= 301 && product_quantity <= 400)
        {
            product_quantity = 6;
            //product_quantity = 1;

        }
        else if(product_quantity >= 400)
        {
            product_quantity = 4;
            // product_quantity = 1;
        }
        else
        {
            product_quantity = 0;
        }

        $("#product_amount").val(product_quantity);


        $("#total_amount").val($("#product_quantity").val()*$("#product_amount").val()+'$');
        $('#amount').val(product_quantity);
        return product_quantity;
    }



</script>

<!-- Scripts a-->

</body>
</html>
