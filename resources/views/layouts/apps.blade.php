
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/sweetalert2/6.4.4/sweetalert2.min.css">
    <style>
        body
        {
            background:#F5F5F5;
        }
        .navbar {
            -moz-border-bottom-colors: none;
            -moz-border-left-colors: none;
            -moz-border-right-colors: none;
            -moz-border-top-colors: none;
            border-color: transparent transparent #ff2200;
            border-image: none;
            margin-bottom: 20px;
            min-height: 139px;
            position: relative;
        }
        .navbar-nav > li > a {
            padding-bottom: 5px;
            padding-top: 5px;
        }
        .navbar-default .navbar-nav > li > a:hover{

            color:#ff2200;
            font-weight:bold;
        }
        .navbar-default {
            background-color: #fff;
            border-color: #e7e7e7;
            box-shadow:0px 1px 6px;
        }
        .navbar-collapse {

            box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1) inset;
            margin-top: 59px;
            overflow-x: visible;
            margin-top: 59px;
            overflow-x: visible;
            padding-left: 120px;
            padding-right: 15px;
            padding-top: 3px;
        }

        .navbar-toggle {
            background-color: transparent;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            float: right;
            margin-bottom: 8px;
            margin-right: 26px;
            margin-top: 43px;
            padding: 9px 10px;
            position: relative;
        }
            </style>
        <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/admin/profile') }}">
                    {{--{{ config('app.name', 'Laravel') }}--}}
                    <img src="{{asset('img/Neurons-inc-consultancy.png')}}" width="110" >
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="/admin/login">Login</a></li>
                        {{--<li><a href="{{ route('register') }}">Register</a></li>--}}
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-user"></i>{{ Auth::user()->username }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="profile" >Profile</a></li>
                                <li>
                                    <a href="/admin/logout') }}"
                                       onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield('header')
    @yield('content')
    @yield('footer')
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://cdn.jsdelivr.net/sweetalert2/6.4.4/sweetalert2.min.js"></script>
{{--<script src="https://code.jquery.com/jquery-3.1.1.js" ></script>--}}

</body>
</html>
