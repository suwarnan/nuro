<div class="col-md-5 col-sm-5 col-xs-12" style="padding:0px; background:#f5f5f5;">

    <div class="panel-default" style="border-bottom:1px solid #999; background:#f5f5f5; height:60px; padding:10px;">
        <div class="dropdown pull-right">
            <a href="{{asset('image_data/'.Auth::user()->username.'/'.$image->folder.'/'.$image->images.'.zip')}}"><button class="btn btn-default" type="button"> Download
                <span class="caret"></span>
            </button></a>
            <!-- <ul class="dropdown-menu">
                <li><a href="{{asset('image_data/'.Auth::user()->username.'/'.$image->folder.'/'.$image->images.'.zip')}}">Download zip file</a></li>

            </ul> -->
        </div>
    </div>

    <div class="panel-default" style="border-bottom:1px solid #999; background:#f5f5f5; height:60px; padding:10px;">
        <i class="fa fa-tasks fa-2x" style="font-size:20px;"> Complexity</i>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-6" style="border:1px solid #ccc">
        <div class="row" style="background:#f5f5f5;">
            <span class="pull-left1">Visual</span>
            <span class="pull-right1 Visual_spam"></span>
            <canvas width=160 height=100 id="Visual_gauge"></canvas>
                 			<span class="pull-right1"></span>

        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6"style="border:1px solid #ccc">
        <div class="row" style="background:#f5f5f5;">
            <span class="pull-left1 ">Saliency</span>
            <span class="pull-right1 Saliency_snam"></span>
            <canvas width=160 height=100 id="Saliency_gauge"></canvas>
                 			<span class="pull-right1"></span>

        </div>
    </div>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
            <div class="panel-default" style="border:1px solid #999;
                    			 border-right:none; background:#f5f5f5; height:60px; padding:10px; margin-bottom:10px;">
                <div class="col-md-6 col-sm-6 col-xs-6"><img src="{{asset('img/bg_color.png')}}" style="position: relative; right: 4%;"/><i  class="fa fa-2x" style="font-size:20px;"> Color load </i>  </div>
                <div class="col-md-6 col-sm-6 col-xs-6"><i  class="fa fa-sun-o fa-2x" style="font-size:20px;"> Brightness</i> </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-6" style="border:0px solid #ccc">
        <div class="row" style="background:#f5f5f5;">
            <div class="col-md-12 col-sm-12 col-xs-12" >
                <div class="col-md-4 col-sm-4 col-xs-4" >
                    <span class="pull-left2" style="padding:2px; height: 40px;">Red</span>
                    <span class="pull-left2" style="padding:2px;height: 40px;">Green</span>
                    <span class="pull-left2" style="padding:2px;height: 40px;">Blue</span>

                </div>
                <div class="col-md-8 col-sm-8 col-xs-8">
                    <div class="progress" style="background-color: #ddd;" >
                    
                        <div class="red" progress=""  >
                         
                        <!-- <span style="margin-left:5px; font-weight:700"></span>  -->
                        </div>
                    </div>
                    <div class="progress" style="background-color: #ddd;">
                        <div class="Green" progress="" >
                            <!-- <span style="margin-left:5px; font-weight:700"></span> -->
                        </div>

                    </div>
                    <div class="progress" style="background-color: #ddd;">
                        <div class="Blue"  process="" >
                            <!-- <span style="margin-left:5px; font-weight:700">5%</span> -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-6">
        <section class="panel" style="background:#f5f5f5;">
            <div class="panel-body">
                <div class="top-stats-panel">
                    <div class="gauge-canvas">
                        <span class="color_snam"></span>
                        <canvas width=160 height=100 id="gauge"></canvas>
                    </div>
                    <ul class="gauge-meta clearfix">
                        <li id="gauge-textfield" class="pull-left gauge-value" style="list-style-type: none;"></li>

                    </ul>
                </div>
            </div>
        </section>
    </div>
    <div class="top-stats-panel">

    </div>

</div>