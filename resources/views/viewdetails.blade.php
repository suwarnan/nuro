@extends('layouts.index')
<link rel="stylesheet" href="{{asset('css/my.css') }}">
<link rel="stylesheet" href="{{asset('css/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="//www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{asset('css/jprogress/jprogress.css')}}">
<style>
#gauge-textfield::after { 
    content: " % ";
}
.pull-left1 {
  float: left !important;
  margin-left: 18px;
}
.pull-left2 {
  float: left !important;
  margin-left: 18px;
  margin-right:3px
}
.pull-right1 {
  float: right !important;
  margin-right: 18px;
}
.nav-pills > li > a {
    border-radius: 0px;
    margin-top:0px;
    margin-bottom:0px;
}
.fa.fa-tasks {
  padding: 10px;
}
.fa.fa-tachometer.fa-5x {
  margin-top: 20px;
  color:#06f;
}
.fa.fa-bar-chart-o.fa-2x {
  padding: 10px;
}
.progress {
  background-color: #f5f5f5;
  border-radius: 0px;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1) inset;
  height: 24px;
  overflow: hidden;
  margin-bottom:0px;
}
.progress-bar {
  background-color: #337ab7;
  box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.15) inset;
  color: #fff;
  float: left;
  font-size: 12px;
  height: 100%;
  line-height: 24px;
  text-align: center;
  transition: width 0.6s ease 0s;
  width: 0;
}
#loading{
    position: fixed;
    top: 50%;
    left: 50%;
    margin-left: -50px; /* half width of the spinner gif */
    margin-top: -50px; /* half height of the spinner gif */
    text-align:center;
    z-index:1234;
    overflow: auto;
    width: 100px; /* width of the spinner gif */
    height: auto; /*hight of the spinner gif +2px to fix IE8 issue */
}
</style>
@section('content')
    <div class="container-fluid" class="col-md-12 col-sm-12 col-xs-12">

        <div class="sm">

            <div class="row" >
                <div class="col-md-1 col-sm-1" style="border:1px solid #C6C6C6; ">
                    <nav class="navbar" style="margin-bottom:0px" >
                        <div class="container-fluid" style="padding:0px;">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="icon-bar" style="background:#999"></span>
                                <span class="icon-bar" style="background:#999"></span>
                                <span class="icon-bar" style="background:#999"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="myNavbar" style="padding:0px;">
                                <ul class="nav nav-stacked" style="padding:0px;">
                                    <li ><a  href="{{url('image')}}" data-title="Back">
                                    <i class="fa fa-fw fa-arrow-left fa-2x" style=""></i></a>
                                    </li>
                                    <li id="original" class="active"><a  href="javascript:void(0)" data-title="Original">
                                            <i class="fa fa-fw fa-picture-o fa-2x" style=""></i></a>
                                    </li>
                                    <li id="heat"><a  href="javascript:void(0)" data-title="heatmap">
                                            <i class="fa fa-fw fa-fire fa-2x" style=""></i></a>
                                    </li>
                                    <li id="fog"><a  href="javascript:void(0)" data-title="fogmap">
                                            <i class="fa fa-fw fa-bars fa-2x" style=""></i></a>
                                    </li>
                                </ul>

                            </div>
                            </div>
                    </nav>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12-" style="border:1px solid #C6C6C6;">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="border:0px solid #C6C6C6">
                            <div class="row original" style="border:5px solid">
                                <img id="image1" src="{{ asset('image_data/'.Auth::user()->username.'/'.$image->folder.'/'.$image->images) }}" class="img-responsive original" />
                            </div>
                            <!-- <div class="row heat" style="border:5px solid">
                            <img src="{{ asset('image_data/'.Auth::user()->username.'/'.$image->folder.'/unzip_'.$image->images.'/'.$image->images.'_heat.jpg') }}" class="img-responsive original" style="height: 80vh !important; width: 80vw !important;"/>
                            </div>
                            <div class="row fog" style="border:5px solid">
                            <img src="{{ asset('image_data/'.Auth::user()->username.'/'.$image->folder.'/unzip_'.$image->images.'/'.$image->images.'_fog.jpg') }}" class="img-responsive original" style="height: 80vh !important; width: 80vw !important;"/>
                            </div> -->
    </div>
    <?php $str = file_get_contents('image_data/'.Auth::user()->username.'/'.$image->folder.'/unzip_'.$image->images.'/'.$image->images.'.json');
    // $str=json_decode(json_encode($str)); 
    $json = json_decode($str, true);  ?>
    
    
    </div>
    </div>@include('rightsidebar')
      </div>
        </div>
        </div>


@endsection



<script src="https://code.jquery.com/jquery-3.1.1.js" ></script>
<script src="{{asset('js/gauge/gauge.js')}}"></script>
<script src="{{asset('js/jprogress/jprogress.js')}}"></script>
<script>
$( document ).ready(function() {
    
    

    $('#original').click(function(){
        
        $("#image1").attr("src","{{ asset('image_data/'.Auth::user()->username.'/'.$image->folder.'/'.$image->images) }}");
        
    });
    $('#heat').click(function(){
        $("#image1").attr("src","{{ asset('image_data/'.Auth::user()->username.'/'.$image->folder.'/unzip_'.$image->images.'/'.$image->images.'_heat.jpg') }}");
        
    });
    $('#fog').click(function(){
        // $('.heat').hide();
        // $('.original').hide();
        // $('.fog').show();
        $("#image1").attr("src","{{ asset('image_data/'.Auth::user()->username.'/'.$image->folder.'/unzip_'.$image->images.'/'.$image->images.'_fog.jpg') }}");
    });
    
});


</script>
<script>
if({{$json["pixels"]["High"]}}>0){
var High = {{ceil($json["pixels"]["High"]* 100 / ($json["pixels"]["Neutral"]  + $json["pixels"]["Static"] + $json["pixels"]["Low"] + $json["pixels"]["High"] + $json["pixels"]["Medium"]))}};
}
else var High={{$json["pixels"]["High"]}};

if({{$json["pixels"]["Medium"]}}>0){
var Medium ={{ceil($json["pixels"]["Medium"]* 100 / ($json["pixels"]["Neutral"]  + $json["pixels"]["Static"] + $json["pixels"]["Low"] + $json["pixels"]["High"] + $json["pixels"]["Medium"]))}};
}
else var Medium ={{$json["pixels"]["Medium"]}};

if({{$json["pixels"]["Low"]}}>0){
var Low = {{ceil($json["pixels"]["Low"]* 100 / ($json["pixels"]["Neutral"]  + $json["pixels"]["Static"] + $json["pixels"]["Low"] + $json["pixels"]["High"] + $json["pixels"]["Medium"]))}};
}
else var Low ={{$json["pixels"]["Low"]}};

if({{$json["pixels"]["Static"]}}>0){
var Static = {{ceil($json["pixels"]["Static"]* 100 / ($json["pixels"]["Neutral"]  + $json["pixels"]["Static"] + $json["pixels"]["Low"] + $json["pixels"]["High"] + $json["pixels"]["Medium"]))}};
}
else var Static={{$json["pixels"]["Static"]}};

if({{$json["pixels"]["Neutral"]}}>0){
var Neutral = {{ceil($json["pixels"]["Neutral"]* 100 / ($json["pixels"]["Neutral"]  + $json["pixels"]["Static"] + $json["pixels"]["Low"] + $json["pixels"]["High"] + $json["pixels"]["Medium"]))}};
}
else var Neutral ={{$json["pixels"]["Neutral"]}};
var Total =  {{$json["pixels"]["Total"]}};
var Saliency_Map = {{number_format($json["complexity"]["Saliency_Map"],2)}};
var Heat_Map = {{number_format($json["complexity"]["Heat_Map"],2)}};
var Image = {{number_format($json["complexity"]["Image"],2)}};
var Blue_Load = {{$json["stats"]["Blue_Load"]}};
var Green_Load = {{$json["stats"]["Green_Load"]}};
var Red_Load = {{$json["stats"]["Red_Load"]}};
var Luminosity = {{$json["stats"]["Luminosity"]}};


$(".Neutral").jprogress({
        background: "#CCCCCC"
        
        });
        $(".Static").jprogress({
        background: "#00CCFF"
        
        });
        $(".Low").jprogress({
        background: "#0099CC"
        
        });
        $(".High").jprogress({
        background: "#003366"
        
        });
        
        $(".red").jprogress({
        background: "#E91C2D"
        
        });
        $(".Green").jprogress({
        background: "#13E900"
        
        });
        $(".Blue").jprogress({
            background: "#4454FF"
        });


       
        
        
    $(function() {      
        if (Gauge) {
    /*Knob*/
    var opts = {
        lines: 12, // The number of lines to draw
        angle: 0, // The length of each line
        lineWidth: 0.48, // The line thickness
        pointer: {
            length: 0.6, // The radius of the inner circle
            strokeWidth: 0.03, // The rotation offset
            color: '#464646' // Fill color
        },
        limitMax: 'true', // If true, the pointer will not go past the end of the gauge
        colorStart: '#00BFFF', // Colors
        colorStop: '#00BFFF', // just experiment with them
        strokeColor: '#F1F1F1', // to see which ones work best for you
        generateGradient: true
    };


    var blue  = {
        lines: 12, // The number of lines to draw
        angle: 0, // The length of each line
        lineWidth: 0.48, // The line thickness
        pointer: {
            length: 0.6, // The radius of the inner circle
            strokeWidth: 0.03, // The rotation offset
            color: '#464646' // Fill color
        },
        limitMax: 'true', // If true, the pointer will not go past the end of the gauge
        colorStart: '#00BFFF', // Colors // just experiment with them
        strokeColor: '#F1F1F1', // to see which ones work best for you
        generateGradient: true
    };
    
    
    //////  Visual
    
 
    var target1 = document.getElementById('Visual_gauge'); // your canvas element
    var gauge = new Gauge(target1).setOptions(opts); // create sexy gauge!
    gauge.maxValue = 100; // set max gauge value
    gauge.animationSpeed = 32; // set animation speed (32 is default value)
    gauge.set(Math.round(Image)); // set actual value
    //gauge.setTextField(document.getElementById("gauge-textfield"));
    //gauge.setTextField(document.getElementById("gauge-textfield"));
    
    
    //////  Saliency
    
    
    var target2 = document.getElementById('Saliency_gauge'); // your canvas element
    var gauge = new Gauge(target2).setOptions(opts); // create sexy gauge!
    gauge.maxValue = 100; // set max gauge value
    gauge.animationSpeed = 32; // set animation speed (32 is default value)
    gauge.set(Math.round(Saliency_Map)); // set actual value
//gauge.setTextField(document.getElementById("gauge-textfield"));
   // gauge.setTextField(document.getElementById("gauge-textfield"));


  /////////////////// last
  
    var total = parseInt(Blue_Load)+ parseInt(Green_Load)+ parseInt(Red_Load);
    var target = document.getElementById('gauge'); // your canvas element
    var gauge = new Gauge(target).setOptions(blue); // create sexy gauge!
    gauge.maxValue = 100; // set max gauge value
    gauge.animationSpeed = 32; // set animation speed (32 is default value)
    gauge.set(total/3); // set actual value
    //gauge.setTextField(document.getElementById("gauge-textfield"));
    gauge.setTextField(document.getElementById("gauge-textfield"));
    //gauge.color('#CCC');
    
    $('.color_snam').html(Math.round(total/3+'%'));
    $('.Saliency_snam').html(Saliency_Map+'%');
$('.Visual_spam').html(Image+'%');
$('.Neutral').attr("progress", Math.round(Neutral)+'%');
$('.Neutral').attr("style", "width:"+Math.round(Neutral)+"%;");
$('.Neutral').attr("style", 'background:#CCCCCC none repeat scroll 0% 0%;');
$('.Neutral span').html(Math.round(Neutral)+'%');
$('.Static').attr("progress", Math.round(Static)+'%');
$('.Static').attr("style", "width:"+Math.round(Static)+"%;");
$('.Static').attr("style", 'background:#00CCFF none repeat scroll 0% 0%;');
$('.Static span').html(Math.round(Static)+'%');
$('.Low').attr("progress", Math.round(Low)+'%');
$('.Low').attr("style", "width:"+Math.round(Low)+"%;");
$('.Low').attr("style", 'background:#003366 none repeat scroll 0% 0%;');
$('.Low span').html(Math.round(Low)+'%');
$('.High').attr("progress", Math.round(High)+'%');
$('.High').attr("style", "width:"+Math.round(High)+"%;");
$('.High').attr("style", 'background:#003366 none repeat scroll 0% 0%;');
$('.High span').html(Math.round(High)+'%');

$('.Blue').attr("progress", Math.round(Blue_Load)+'%');
$('.Blue').attr("style", "width:"+Math.round(Blue_Load)+"%;background:#4454FF;");
$('.Blue').html(Math.round(Blue_Load)+'%');
$('.Green').attr("progress", Math.round(Green_Load)+'%');
$('.Green').attr("style", "width:"+Math.round(Green_Load)+"%;background:#13E900;");
$('.Green').html(Math.round(Green_Load)+'%');
$('.red').attr("progress", Math.round(Red_Load)+'%');
$('.red').attr("style", "width:"+Math.round(Red_Load)+"%;background:#E91C2D;");
$('.red').html(Math.round(Red_Load)+'%');
}
});

</script>


