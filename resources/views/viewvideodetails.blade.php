@extends('layouts.index')
<link rel="stylesheet" href="{{asset('css/my.css') }}">
<link rel="stylesheet" href="{{asset('css/font-awesome-4.7.0/css/font-awesome.min.css') }}">
@section('content')

<!-- <div class="col-md-12 col-sm-12" style="border:1px solid #C6C6C6;"> -->
    <div class="col-md-12 col-sm-12 col-xs-12" style="padding:0px; /* background-image:url(images/p3.png); */ background:#f5f5f5;">

        <div class="panel-default" style="border-bottom:1px solid #999; background:#f5f5f5; height:60px; padding:10px;">
            <div class="dropdown pull-right">
                <a href="{{asset('video_data/'.Auth::user()->username.'/'.$videos->folder.'/'.$videos->video_name.'.zip')}}"><button class="btn btn-default " type="button" > Download
                    <span class="caret"></span>
                </button></a>
                <ul class="dropdown-menu">
                    <li><a href="{{asset('video_data/'.Auth::user()->username.'/'.$videos->folder.'/'.$videos->video_name.'.zip')}}">Download zip file</a></li>

                </ul>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-1 col-sm-1" >
            <nav class="navbar" style="margin-bottom:0px" >
                <div class="container-fluid" style="padding:0px;">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar" style="background:#999"></span>
                        <span class="icon-bar" style="background:#999"></span>
                        <span class="icon-bar" style="background:#999"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="myNavbar" style="padding:0px;">
                        <ul class="nav nav-stacked" style="padding:0px;">
                            <li class="active"><a  href="{{url('video')}}" data-title="Back">
                                    <i class="fa fa-fw fa-arrow-left fa-2x" style=""></i></a>
                            </li>
                            {{--<li id="original"><a  href="javascript:void(0)">--}}
                                    {{--<i class="fa fa-fw fa-picture-o fa-2x" style=""></i></a>--}}
                            {{--</li>--}}
                            {{--<li id="heat"><a  href="javascript:void(0)">--}}
                                    {{--<i class="fa fa-fw fa-fire fa-2x" style=""></i></a>--}}
                            {{--</li>--}}
                            {{--<li id="fog"><a  href="javascript:void(0)">--}}
                                    {{--<i class="fa fa-fw fa-bars fa-2x" style=""></i></a>--}}
                            {{--</li>--}}
                        </ul>

                    </div>
                </div>
            </nav>
        </div>
        <div class="col-md-7 col-sm-11 col-xs-12 " style="border:0px solid #C6C6C6">


                <div class="row videooriginal" style="border:1px solid;">
                    <video width="400" controls class="img-responsive orignal" alt="" style="margin-bottom:10px;" >
                        <source src="{{asset('video_data/'.Auth::user()->username.'/'.$videos->folder.'/'.$videos->video_name)}}">

                        Your browser does not support HTML5 video.
                    </video>
                </div>
            <div class="row videoheat" style="border:1px solid;" >
                <video width="400" controls class="img-responsive" alt="" style="margin-bottom:10px;" >
                    <source type="video/webm" src="{{asset('video_data/'.Auth::user()->username.'/'.$videos->folder.'/unzip_'.$videos->video_name.'/'.explode('.', $videos->video_name)[0].'_recoded.mp4_heat_recoded.mp4')}}">

                    Your browser does not support HTML5 video.
                </video>
            </div>
        </div>
    </div>

<div class="col-md-12 col-sm-12" style="border:20px solid #C6C6C6;"></div>
@endsection
<script src="https://code.jquery.com/jquery-3.1.1.js" ></script>
<script>
    $('#loading').hide();
    $('.heat').hide();
    $('.fog').hide();
    $('.videoheat').hide();

    $('#original').click(function(){
        $('.heat').hide();
        $('.fog').hide();
        $('.original').show();
        $('.videooriginal').show();
        $('.videoheat').hide();
    });
    $('#heat').click(function(){
        $('.videooriginal').hide();
        $('.original').hide();
        $('.fog').hide();
        $('.heat').show();
        $('.videoheat').show();
    });
    $('#fog').click(function(){
        $('.heat').hide();
        $('.original').hide();
        $('.fog').show();
    });


</script>