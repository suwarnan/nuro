@if(count($images)>0)
    @foreach($images as $image)
<div class="col-md-3 col-sm-6 col-xs-10" style="padding:15px;" id="{{$image->id}}">

    <ul class="caption-style-1">
        <li>
            <div class="caption">
                <div class="blur"></div>
                <div class="caption-text">
                    <img onclick="deleteimage({{$image->id}})" src="{{asset('img/Delete.png')}}" width="50" data-toggle="tooltip" title="Delete Picture" data-placement="right" />


                </div>
            </div>
            <form id="myform.{{$image->id}}" method="post" action="{{url('/image/viewdetails')}}">
                <input type="hidden" name="image" value="{{$image->id}}" />
                <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">

                <a onclick="document.getElementById('myform.{{$image->id}}').submit();" data-toggle="tooltip"
                   class="@if($image->download ==2 ){{"link"}} @else{{"notlink"}} @endif">

                    <img src="{{ asset('image_data/'.Auth::user()->username.'/'.$image->folder.'/thumb/'.$image->images) }}" width="200" height="200"  class="@if($image->download ==2 ){{ "link"}}@else{{ "notlink"}} @endif"/>
                    <div class="caption" >
                        <div class="blur"></div>
                        <div class="caption-text" style="top:45px">
                            <h1>View Results</h1>

                        </div>
                    </div>
                </a>
            </form>
        </li>
    </ul>
</div>
@endforeach

@else
    <div class="alert alert-danger col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
    <strong> Data Not found . Please upload some images </strong>
</div>
@endif
