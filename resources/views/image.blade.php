@extends('layouts.index')
<link rel="stylesheet" href="{{asset('css/my.css') }}">

<link rel="stylesheet" href="{{asset('packages/dropzone/dropzone.css') }}">
<script src="{{asset('packages/dropzone/dropzone.js') }}"  ></script>
@section('content')

    {{--<p>     {{ Auth::user()->id }} </p>--}}
    <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
    <div class="col-md-12 col-sm-12" style="border:1px solid #C6C6C6;">
        <div class="row" style="padding-left:0px">
            <div class="col-md-1 col-sm-1" style="padding:0px;border:1px solid #C6C6C6; ">
                <nav class="navbar" style="margin-bottom:0px">
                    <div class="container-fluid" style="padding:0px;">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar" style="background:#999"></span>
                            <span class="icon-bar" style="background:#999"></span>
                            <span class="icon-bar" style="background:#999"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="myNavbar" style="padding:0px;">
                            <ul class="nav nav-stacked" style="padding:0px;">
                                <li class="active"><a  href="{{url('image')}}" class="active" data-title="All Images">

                                        <img src="{{asset('img/1457439811_album_gallery_image_images_photo_photos_pictures_portfolio.png')}}" width="40" height="40"   data-toggle="tooltip" title="All Images" data-placement="right"/>

                                    </a>
                                </li>
                                <li ><a  href="{{url('video')}}" data-title="All Videos" >

                                        <img src="{{asset('img/1457440126_24.TV.png')}}" width="40" height="40" data-toggle="tooltip" title="All Videos" data-placement="right"/>
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12" style="border:1px solid #C6C6C6">
                <div class="row" style="border:1px solid #C6C6C6" >
                <div class="col-md-10 col-sm-10 col-xs-10" style="padding-top:15px; ">
                    <form >
                        <select class="form-control " name="list"  id="folderlist">
                            @if(count($folders) > 0)
                            @foreach($folders as $folder)
                                @if($folder!='.'||$folder!='..')

                        <option value="{{$folder}}"  @if( session()->get('uploadFolder')==$folder)  {{"selected"}} @endif>{{$folder}}</option>
                               @endif
                                @endforeach
                                @else
                                <option value="">Please create your directory </option>
                                @endif


                        </select>

                    </form>
                </div>

                <div class="col-md-1 col-sm-1 col-xs-1" style="padding-left:7px;padding-top:15px;">
                    <a href="#addfolder" data-toggle="modal" ><i class="glyphicon glyphicon-plus" data-toggle="tooltip" title="Create Folder" data-placement="top"></i></a>



                </div>
                <div class="col-md-1 col-sm-1 col-xs-1" style="padding-left:5px;padding-top:15px;">
                    <a href="javascript:void(0);" onclick="deletefolder()"><i class="glyphicon glyphicon-remove" data-toggle="tooltip" title="Delete Folder" data-placement="top"></i></a>
                </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12" id="uploadimagecontainer" style="padding:15px;height:80%">

                    <div class="dropzone chcolor-tooltip " id="dropzoneFileUpload">
                        <h4 style="text-align: center; color:#428bca;">Drop images in this area  <span class="glyphicon glyphicon-hand-down"></span></h4>
                    </div>
                    {{--<button type="button" id="uploadPhoto">Upload Photo</button>--}}
                </div>
            </div><!--col-3 end-->
            <div class="col-md-8 col-sm-8" style="padding-top:30px;padding-left:0px;">
                <div class="addimage" id="addimagess" style="background-image:url(/img/p3.png);">

                    @include('images')

                </div>

            </div>
        </div>
</div>

    <div id="addfolder" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Create folder</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{url('/cfolder')}}" id="CreateForm" >
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="text" name="image_folder" class="form-control" id="image_folder" placeholder="Folder Name" required>
                            </div>
                        </div>
                        <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="submit"  value="Create"  id="Create" class="btn btn-primary " style="background:#01A156;height:50px; margin-bottom:15px;width:37%">
                        </div>
                    </form>
                </div>

                <div style="clear:both;"></div><div id="message"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>

       

    


   
<script src="https://code.jquery.com/jquery-3.1.1.js" ></script>
    <script>
  var ajax_call = function() {
        var crsfToken = $( "#_token" ).val();
        $.ajax({
            url : "{{url('/receive')}}", // use your target
            type : "POST",
            data : "download=download",

            headers: {
                "X-CSRF-TOKEN": crsfToken
            },
            success : function(data) {
//                alert(data);
                $.ajax({
                    url :"{{url('/extract')}}", // use your target
                    type : "POST",
                    data : "download=download",

                    headers: {
                        "X-CSRF-TOKEN": crsfToken
                    },
                    success : function(data)
                    {

//                        alert(data);
                        // location.reload();
                    $("#addimagess").load(location.href + " #addimagess");
                    }

                });
            }
        });
    };
    var interval = 100 * 60 * 1; // where X is your every X minutes

            setInterval(ajax_call, interval);

    function deletefolder()
    {
        var name = $('select[name=list]').val();
        var crsfToken = $( "#_token" ).val();
        if(name == "")
        {
            alert("Folder name not found . Please Create a folder first");
        }
        else
        {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {
                $.ajax({
                    url : '{{url("/deletefolder")}}', // use your target
                    type : "POST",
                    data : {"name":name},
                    headers: {
                        "X-CSRF-TOKEN": crsfToken
                    },
                    success : function(data) {
                        swal(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                        );
                        location.reload();
                    }
                });
            });
        }

    }
    function deleteimage(id)
    {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then(function () {
            var crsfToken = $( "#_token" ).val();
            $.ajax({
                url : '{{url("/Deleteimage")}}', // use your target
                type : "POST",
                data : "imageid="+id,
                headers: {
                    "X-CSRF-TOKEN": crsfToken
                },
                success : function(data) {
                    if(data == 1)
                    {
                        swal(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                        );
                        $("#"+id).remove();
                    }
                }
            });
        });

    }

    $(document).ready(function() {
        $("#folderlist").change(function () {
            var crsfToken = $( "#_token" ).val();
            folder=$("#folderlist").val();
            $.ajax({
                type: "POST",
                url: '{{url("/changefolder")}}',
                data: 'image_folder='+folder,
                headers: {
                    "X-CSRF-TOKEN": crsfToken
                },
                success: function(data)
                {
                    window.location ='/image';

                }
            });

        });

    });
        $("#Create").click( function(){
            var image_folder=$("#image_folder").val();
//            var dataString = 'image_folder='+image_folder;
            var crsfToken = $( "#_token" ).val();
            if($.trim(image_folder).length>0)
            {
                $.ajax({
                    type: "POST",
                    url: '{{url("/cfolder")}}',
                    data: 'image_folder='+image_folder,
                    headers: {
                        "X-CSRF-TOKEN": crsfToken
                    },
                    success: function(data)
                    {
                        $("#message").html(data).fadeIn(6000).fadeOut(4000);
                    }
                });
                return false;
            }
            return false;

        });


    </script>
    <script>

        Dropzone.autoDiscover = false;
        jQuery(document).ready(function(){
            var myDropzone = new Dropzone("div#dropzoneFileUpload", {
                url: "{{url('/uploadFiles')}}",
                maxFilesize: 20, // MB
                maxFiles: 10,
                addRemoveLinks: false,
                autoProcessQueue:true,
                acceptedFiles:'image/*',
                params: {
                    _token: "{{csrf_token()}}"
                },
                 init : function(){
                        this.on("error", function(file)
                        {
                        if(!file.accepted){
                        swal({
                            title: 'Error!',
                            text: 'You cannot upload this file',
                            type: 'error',
                            confirmButtonText: 'OK'

                        })
                        this.removeFile(file);
                        }
                        });
                     }

            });

            myDropzone.on("success", function(file,resp){
                if(resp=="success"){
//                    alert("Image uploaded successfully");
                    swal({
                        title: 'Success!',
                        text: 'Image uploaded Successfully',
                        type: 'success',
                        confirmButtonText: 'OK'
                    });
                //     .then(
                //             function () {
                //                 window.location ='/image';
                //             });
                 }
                else {
//                    alert(resp);
                    swal({
                        title: 'Error!',
                        text: resp,
                        type: 'error',
                        confirmButtonText: 'OK'

                         })
                    // .then(
                    //         function () {
                    //             window.location ='/image';
                    //         });


                }
            });
            myDropzone.on("complete", function(file) {
             myDropzone.removeFile(file);
             
            });

            jQuery("button#uploadPhoto").click(function(){
                myDropzone.processQueue();
            });
        });


    </script>

@endsection
