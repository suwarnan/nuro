@if(count($videos)>0)
<div style="white-space: nowrap;display:block;">

    @foreach($videos as $video)
   
        <div class="col-md-4 col-sm-6  @if($video->download ==2 )
        {{"link" }}@else{{ "notlink" }} @endif"  style="padding:15px; position: relative;" id="{{ $video->id }}">
        

            <video   width="300" height="120" class="img-responsive" alt="" style="margin-bottom:10px;border: 1px solid black;
    outline-style: double;
    outline-color: red;" controls>
                <source src="{{asset('video_data/'.Auth::user()->username.'/'.$video->folder.'/'. $video->video_name.'#t=0.8')}}" type="video/mp4" />

                Your browser does not support HTML5 video.
            </video>
             @if($video->download ==2 )
            <form id="{{'myform'.$video->id}}" method="post" action="video/viewdetails">
                <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="video" value="{{$video->id}}" />

                <button type="submit" style="border:none;"  @if($video->download ==2 ){{""}}@else{{ "disabled"}} @endif > View Results</button>

               @endif
                <a href="javascript:void(0)" class="pull-right" style="opacity: 1;overflow: inherit; pointer-events: auto; position: relative;" onclick="deletevideo({{$video->id}})" data-toggle="tooltip" title="Delete Video" >Delete Video</a>
            </form>

            


            </div>
           
    @endforeach
</div>
@else
    <div class="alert alert-danger col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
        <strong> Data Not found . Please upload some videos </strong>
    </div>
@endif
