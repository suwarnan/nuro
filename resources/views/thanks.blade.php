<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {!! HTML::style('/packages/bootstrap/css/bootstrap.min.css') !!}
    <link rel="stylesheet" href="//cdn.jsdelivr.net/sweetalert2/6.4.4/sweetalert2.min.css">
<script>
function redirect(path)
{

setTimeout(function(){ window.location = path; }, 5000);
}
</script>
</head>
<body>
<div id="wizard1-4"   style="
  " class="tab-pane  text-center active">
                        <h1 class="thin" style="margin-bottom: 18px"> </h1>
                      
            <div class="col-md-12" style="padding-bottom:3%;">
            
            <div class="">
                         <img onload="redirect('{{url('/home')}}" src="images/Neurons-inc-consultancy.png" width="55%">
             
             </div>
             
             </div>
                        <h2 class="semi-bold">Thank you for your purchase, you are being transferred back to the NeuroVision page</h2>
                    </div>
</body>
</html>
