-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2017 at 08:38 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nuro`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_03_04_054837_create_refcode_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `refcode`
--

CREATE TABLE `refcode` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `refcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `refcode`
--

INSERT INTO `refcode` (`user_id`, `refcode`, `type`, `created_at`, `updated_at`) VALUES
(10, '58ba6c4358c81', 1, '2017-03-04 01:56:59', '2017-03-04 01:56:59'),
(12, '58ba6ea91816b', 1, '2017-03-04 02:07:13', '2017-03-04 02:07:13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Block','Unblock') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Block',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `firstname`, `lastname`, `country`, `company`, `status`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(10, 'NeuronsInc', 'tester', 'tester1', 'Australia', 'abcd', 'Block', 'skan@irstha.com', '$2y$10$IcOD.P5kavOcEVp81LPv7O3CfKbFnm7YvQWIqWlrSawKHI1wtmD7a', NULL, '2017-03-04 01:56:59', '2017-03-04 01:56:59'),
(11, 'NeuronsIncs', 'tester', 'dfsg', 'Aruba', 'zxczxc', 'Block', 'skan1@irstha.com', '$2y$10$1sYkE2mLgvPLmb0uTkKJrOulGzghMG31L5BDKYYWW7XetAv2VHDT.', 'nhAzpV3AzV4r99zllGkhBjoIPm4b3C6ILPAKlJJaY1qI6fTjZHQkONcLNATh', '2017-03-04 01:58:53', '2017-03-04 01:58:53'),
(12, 'test', 'tester', 'xcbfx', 'Bahamas', 'abcd', 'Block', 'ra@gmail.com', '$2y$10$9TB.ByZJtCTfNzoJy9LpueobxXTQilP8wea2yBqgOKFzoq/D7tecm', NULL, '2017-03-04 02:07:13', '2017-03-04 02:07:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refcode`
--
ALTER TABLE `refcode`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `refcode`
--
ALTER TABLE `refcode`
  ADD CONSTRAINT `refcode_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
