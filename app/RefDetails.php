<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefDetails extends Model
{
    protected $table = 'ref_code';
    public $timestamps = false;
    protected $fillable = [
        'user_id','owner_id','status',
    ];
}
