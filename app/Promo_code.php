<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo_code extends Model
{
    protected $table = 'promo_code';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'id','name','promocode','discount','active',
    ];
}
