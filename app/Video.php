<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'video';
    public $timestamps = false;
    protected $fillable = [
        'id','user_name','video_name','uploaded','folder','download','bitrate',
    ];

}
