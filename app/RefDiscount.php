<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RefDiscount extends Model
{
    protected $table = 'ref_discount';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'id','Name','user_discount','owner_discount','active',
    ];
}
