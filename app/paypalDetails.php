<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paypalDetails extends Model
{
    protected $table = 'ibn_table';
    public $timestamps = false;
    protected $fillable = [
        'id','itransaction_id','ipayerid','iname','iemail','ipaymentstatus','ieverything_else','itransaction_date',
    ];
}
