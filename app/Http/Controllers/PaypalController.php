<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Paypal;
use App\paypalDetails;
use App\promoUser;
use App\Credit;
use App\RefDetails;
use App\Refcode;
use App\RefDiscount;
use App\ownerRef;
use App\userRef;
use Illuminate\Support\Facades\Auth;
class PaypalController extends Controller
{
    private $_apiContext;
    public function __construct()
    {
        $this->_apiContext = PayPal::ApiContext(
            config('services.paypal.client_id'),
            config('services.paypal.secret'));

        $this->_apiContext->setConfig(array(
            'mode' => 'sandbox',
            'service.EndPoint' => 'https://api.sandbox.paypal.com',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
        ));

    }


    public function getCheckout(Request $request){
//    var_dump($request->get('item_name'));
        $payer = PayPal::Payer();
        $payer->setPaymentMethod('paypal');


        
        $amount = PayPal:: Amount();
        $amount->setCurrency('USD');
        $amount->setTotal($request->get('amount')*$request->get('quantity'));
//        $amount->setDetails($request->get('discount_amount2'));


        $describe=array('credit'=>$request->get('quantity'),'discount'=>$request->get('discount_amount2'));
        $transaction = PayPal::Transaction();
        $transaction->setAmount($amount);
        $transaction->setDescription(json_encode($describe));

        $redirectUrls = PayPal:: RedirectUrls();
        $redirectUrls->setReturnUrl(route('getDone'));
        $redirectUrls->setCancelUrl(route('getCancel'));

        $payment = PayPal::Payment();
        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array($transaction));

        $response = $payment->create($this->_apiContext);
        $redirectUrl = $response->links[1]->href;

        return redirect()->to( $redirectUrl );
}
    public function getDone(Request $request)
    {
        $id = $request->get('paymentId');
        $token = $request->get('token');
        $payer_id = $request->get('PayerID');

        $payment = PayPal::getById($id, $this->_apiContext);

        $paymentExecution = PayPal::PaymentExecution();

        $paymentExecution->setPayerId($payer_id);
        $executePayment = $payment->execute($paymentExecution, $this->_apiContext);

        $pay=new paypalDetails();
        $pay->itransaction_id=$id;
        $pay->ipayerid=$payer_id;
        $pay->iname=$executePayment->payer->payer_info->first_name;
        $pay->iemail=$executePayment->payer->payer_info->email;
        $pay->ipaymentstatus='pending';
        $pay->ieverything_else=$executePayment;
        $pay->itransaction_date=date("Y-m-d h:i:s");
        $pay->save();

        $total=$executePayment->transactions[0]->amount->total;
        $description=$executePayment->transactions[0]->description;
        $descript=json_decode($description);
        $credit=$descript->credit;
        $discount=$descript->discount;
        $credits=Credit::where('user_id',Auth::user()->id)
            ->get();
        $add_credits = Credit::where('user_id', Auth::user()->id)
            ->update(['total_credit' => $credits[0]->total_credit+$credit]);
        if($discount>0){
            $promo=new promoUser();
            $promo->user_id=Auth::user()->id;
            $promo->Date=date("Y-m-d h:i:s");
            $promo->token=$credit;
            $promo->promo_token=$discount;
            $promo->save();
        }

        $refdetails=RefDetails::where('user_id',Auth::user()->id)
            ->where('status','pending')
            ->get();
        if(count($refdetails)>0)
        {
            $refcode=Refcode::where('user_id',Auth::user()->id)
                ->get();
            $type=$refcode[0]->type;

            $refdiscount=RefDiscount::where('id',$type)
                ->where('active','active')
                ->get();
            if(count($refdiscount)>0) {
                //owner
                $owner_old_credits = Credit::where('user_id',$refdetails[0]->owner_id )
                    ->get();
                $update_owner_credit=Credit::where('user_id', $refdetails[0]->owner_id)
                    ->update(['total_credit' => $owner_old_credits[0]->total_credit+$refdiscount[0]->owner_discount]);

                $update_owner_ref=new ownerRef();
                $update_owner_ref->user_id= $refdetails[0]->owner_id;
                $update_owner_ref->earned_amount=$refdiscount[0]->owner_discount;
                $update_owner_ref->date=date("Y-m-d h:i:s");
                $update_owner_ref->save();

                //user
                $user_old_credits = Credit::where('user_id',Auth::user()->id )
                    ->get();
                $update_user_credit=Credit::where('user_id', Auth::user()->id)
                    ->update(['total_credit' => $user_old_credits[0]->total_credit+$refdiscount[0]->user_discount]);

                $update_user_ref=new userRef();
                $update_user_ref->user_id= Auth::user()->id;
                $update_user_ref->earned_amount=$refdiscount[0]->user_discount;
                $update_user_ref->date=date("Y-m-d h:i:s");
                $update_user_ref->save();

                //close ref discount
                $update_status=RefDetails::where('user_id', Auth::user()->id)
                    ->update(['status' => 'finished']);
            }

        }

//        print_r($descript->credit);
        return redirect('home');
    }
    
    public function getCancel()
    {
        echo "cancel";
        return redirect('home');
//        return redirect()->route('payPremium');
    }
}
