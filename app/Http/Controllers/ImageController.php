<?php

namespace App\Http\Controllers;

use App\Credit;
use App\Logic\Image\ImageRepository;
use App\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use getID3;
use File;
use App\Image;
use App\Refcode;
use App\RefDetails;
use App\RefDiscount;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManager;
use Chumper\Zipper\Zipper;
use ZipArchive;

class ImageController extends Controller
{
    public function __construct()
    {

        ini_set('upload_max_filesize', '1000M');
        ini_set('post_max_size', '1000M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    }

    public function getUpload()
    {
        return view('image');
    }

    
    public function uploadFiles(Request $request){
    $image = $request->file('file');
    
    if($image!="") {
        $img = \Image::make($image);
        $ext=array("png","jpeg","jpg","bmp","PNG","JPG");
    
   //     $getID3 = new getID3;
//        $analyse=$getID3->analyze($request->file('file'));
        if(in_array($image->getClientOriginalExtension(),$ext)) {
            $originalNameWithoutExt = substr($image->getClientOriginalName(), 0, strlen($image->getClientOriginalName()) - strlen($image->getClientOriginalExtension()) - 1);
            $filename = $this->sanitize($originalNameWithoutExt);
            $imageName = time() .$filename.'.jpg';//.$image->getClientOriginalExtension();
            if(session()->get('uploadFolder')!='') {
                $image->move('image_data/' . Auth::user()->username . '/' . session()->get('uploadFolder'), $imageName);
                 
                $img->resize(150,100);
                if(!File::exists('image_data/' . Auth::user()->username . '/' . session()->get('uploadFolder').'/thumb')) {
                    File::makeDirectory('image_data/' . Auth::user()->username . '/' . session()->get('uploadFolder').'/thumb', 0777, true);
                }
                $img->save('image_data/' . Auth::user()->username . '/' . session()->get('uploadFolder').'/thumb'.'/'. $imageName);


                $credits = Credit::where('user_id', Auth::user()->id)
                    ->get();

                if ($credits[0]->total_credit >= 1) {
                    $images = new Image;

                    $images->user_name = Auth::user()->id;
                    $images->images = $imageName;
                    $images->folder = session()->get('uploadFolder');
                    $images->download = 0;
                    $images->save();
                    $credit = Credit::where('user_id', Auth::user()->id)
                        ->update(['total_credit' => $credits[0]->total_credit - 1]);
                    if (!File::copy('image_data/' . Auth::user()->username . '/' . session()->get('uploadFolder') . '/' . $imageName, '/home/img/img1' . '/' . $imageName)) {
                        die("Couldn't copy file");
                    }
//            return json_encode(['success'=>$analyse]);
                    return "success";
                } else {
                    return "not enough credit";
                }
            }
            else return "Please create directory";
        }
        else
            return "WRONG EXTENSION FILE";
    }
    else return "WRONG EXTENSION UPLOADED";
    }

    public function createfolder(Request $request){
        $filename = $this->sanitize1($request->get('image_folder'),false);
        if($filename!="")
        File::makeDirectory('image_data/'. Auth::user()->username.'/'.$filename.'/thumb', 0777, true);

        return redirect('image');
    }

    public function changefolder(Request $request){
        session()->put('uploadFolder',$request->get('image_folder'));
        echo "success";
//        return redirect('image');
    }
    public function Deleteimage(Request $request){
        $image = Image::find( $request->get('imageid'));
        $image->delete();
        echo 1;

    }
    public function deletefolder(Request $request){
        $deletedRows = Image::where('folder',$request->get('name'))->delete();
//        File::copyDirectory($sourceDir, $destinationDir);
        File::deleteDirectory('image_data/'. Auth::user()->username.'/'.$request->get('name'));
        session()->put('uploadFolder')=='';
        echo "success";
    }

    public function viewdetails(Request $request) {
        $refcode = Refcode::where('user_id', Auth::user()->id)
            ->get();

        $credit=Credit::where('user_id', Auth::user()->id)
            ->get();

        $refdetails=RefDetails::where('user_id', Auth::user()->id)
            ->where('status','pending')
            ->get();
        if(count($refdetails)>0) {
            $refowner = User::where('id', $refdetails[0]->owner_id)->get();
        }
        else {
            $refdetails="";
            $refowner ="";
        }
        $refdiscount=RefDiscount::where('id',$refcode[0]->type)->get();

        $image=Image::find($request->get('image'));

        return view('viewdetails')->with('image',$image)
            ->with('refcode',$refcode)
            ->with('credit',$credit)
            ->with('refdetails',$refdetails)
            ->with('refdiscount',$refdiscount)
            ->with('refowner',$refowner);

    }

    function sanitize($string, $force_lowercase = true, $anal = false)
    {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+","-", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€”", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;

        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }
    function sanitize1($string, $force_lowercase = true, $anal = false)
    {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+","-", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€”", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;

        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }

    function receive(Request $request){
        $images=Image::where('download',0)
                      ->where('user_name',Auth::user()->id)
                      ->get();

        foreach($images as $image){
            if(File::exists('/home/img/img2/'.$image->images.'.'.'zip'))
            if(File::copy('/home/img/img2/'.$image->images.'.'.'zip', 'image_data/'.Auth::user()->username.'/'.$image->folder.'/'.$image->images.'.'.'zip')){
                $a = Image::where('id', $image->id)
                    ->update(['download' => 1]);
            }

        }
        echo "success";

    }

    function extract(Request $request)
    {

        $images = Image::where('download', 1)
            ->where('user_name', Auth::user()->id)
            ->get();
        foreach ($images as $image) {
            $zip = new ZipArchive();
            $res = $zip->open('image_data/' . Auth::user()->username . '/' . $image->folder.'/'.$image->images.'.'.'zip');
            if ($res === TRUE) {

                $zip->extractTo('image_data/' . Auth::user()->username . '/' . $image->folder.'/unzip_'.$image->images);
                $zip->close();

                $a = Image::where('id', $image->id)
                    ->update(['download' => 2]);
            }
        }
        echo "success";
    }

}
