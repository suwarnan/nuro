<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use App\Credit;

use App\User;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use DB;
use getID3;
use File;
use App\Image;
use App\Refcode;
use App\RefDetails;
use App\RefDiscount;

use Illuminate\Support\Facades\Validator;
use Chumper\Zipper\Zipper;
use ZipArchive;
class VideoController extends Controller
{
    public function __construct()
    {

        ini_set('upload_max_filesize', '1000M');
        ini_set('post_max_size', '1000M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    }
     public function viewdetails(Request $request){
         $refcode = Refcode::where('user_id', Auth::user()->id)
             ->get();

         $credit=Credit::where('user_id', Auth::user()->id)
             ->get();

         $refdetails=RefDetails::where('user_id', Auth::user()->id)
             ->where('status','pending')
             ->get();
         if(count($refdetails)>0) {
             $refowner = User::where('id', $refdetails[0]->owner_id)->get();
         }
         else {
             $refdetails="";
             $refowner ="";
         }
         $refdiscount=RefDiscount::where('id',$refcode[0]->type)->get();

         $video=Video::find($request->get('video'));
         return view('viewvideodetails')
             ->with('refcode',$refcode)
             ->with('credit',$credit)
             ->with('refdetails',$refdetails)
             ->with('refdiscount',$refdiscount)
             ->with('refowner',$refowner)
             ->with('videos',$video);
     }


    public function createfolder(Request $request){
        $filename = $this->sanitize1($request->get('video_folder'),false);
        if($filename!="")
        File::makeDirectory('video_data/'. Auth::user()->username.'/'.$filename, 0777, true);
    
        return redirect('video');
    }
    public function changefolder(Request $request){
        session()->put('uploadvideoFolder',$request->get('video_folder'));
        echo "success";
//        return redirect('image');
    }
    public function Deletevideo(Request $request){
        $image = Video::find( $request->get('videoid'));
        $image->delete();
        echo 1;

    }
    public function deletefolder(Request $request){
        $deletedRows = Video::where('folder',$request->get('name'))->delete();
//        File::copyDirectory($sourceDir, $destinationDir);
        File::deleteDirectory('video_data/'. Auth::user()->username.'/'.$request->get('name'));
        session()->put('uploadvideoFolder')=='';
        echo "success";
    }

    public function uploadFiles(Request $request){

//        $path = base_path()."/public/storage/";
        $video = $request->file('file');
        if($video!=""){
        $ext=array("mp4","3gp");
        if(in_array($video->getClientOriginalExtension(),$ext)) {
            $getID3 = new getID3;
            $analyse=$getID3->analyze($request->file('file'));
            $totalsize = $analyse['playtime_seconds'];
            $bitrate = $analyse["video"]["resolution_x"];
            if($totalsize > 35)
            {
                $duecredit = ceil($totalsize/35)*10;
            }
            else
            {
                $duecredit = 10;
            }
            $originalNameWithoutExt = substr($video->getClientOriginalName(), 0, strlen($video->getClientOriginalName()) - strlen($video->getClientOriginalExtension()) - 1);
            $filename = $this->sanitize($originalNameWithoutExt);
            $videoName = time() .$filename.'.'.$video->getClientOriginalExtension();
            if(session()->get('uploadvideoFolder')!='') {
              
                
                $video->move('video_data/' . Auth::user()->username . '/' . session()->get('uploadvideoFolder'), $videoName);

                $credits = Credit::where('user_id', Auth::user()->id)
                    ->get();

                if ($credits[0]->total_credit >= $duecredit && $bitrate >= 640) {

                    DB::table('video')->insert(
                        ['user_name' => Auth::user()->id, 'video_name' => $videoName, 'uploaded' => date("Y-m-d H:i:s"), 'folder' => session()->get('uploadvideoFolder'),
                            'download' => 0, 'bitrate' => $bitrate]
                    );

                    $credit = Credit::where('user_id', Auth::user()->id)
                        ->update(['total_credit' => $credits[0]->total_credit - $duecredit]);
                    if (!File::copy('video_data/' . Auth::user()->username . '/' . session()->get('uploadvideoFolder') . '/' . $videoName, '/home/vid/vid1' . '/' . $videoName)) {
                        die("Couldn't copy file");
                    }
//            return $analyse;
                    return "success";
                } else {
                    return "lowcredit or low bitrate";
                }
            }
            else return "Please include folder";
        }
        else
            return "false";
    }
    else return "False";
    }

    function sanitize($string, $force_lowercase = true, $anal = false)
    {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+","-", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€”", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;

        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }
    function sanitize1($string, $force_lowercase = true, $anal = false)
    {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+","-", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€”", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;

        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }


    function receive(Request $request){
        $videos=Video::where('download',0)
            ->where('user_name',Auth::user()->id)
            ->get();
        if(count($videos)>0){
        foreach($videos as $video){
            $name = explode(".", $video->video_name);
            if(File::exists('/home/vid/vid2/'.$name[0].'_recoded.mp4.zip'))
            if(File::copy('/home/vid/vid2/'.$name[0].'_recoded.mp4.zip', 'video_data/'.Auth::user()->username.'/'.$video->folder.'/'.$video->video_name.'.zip')){
                $a = Video::where('id', $video->id)
                    ->update(['download' => 1]);
            }

        }
        }
        echo "success";

    }

    function extract(Request $request)
    {

        $videos = Video::where('download', 1)
            ->where('user_name', Auth::user()->id)
            ->get();
        foreach ($videos as $video) {
             $zip = new ZipArchive();
            $res = $zip->open('video_data/' . Auth::user()->username. '/' . $video->folder. '/' .$video->video_name. '.zip');
            if ($res === TRUE) {
                $zip->extractTo('video_data/'.Auth::user()->username.'/'.$video->folder.'/unzip_'.$video->video_name);
                $zip->close();
                $a = Video::where('id', $video->id)
                    ->update(['download' => 2]);
            }
        }
        echo "success";
    }
}
