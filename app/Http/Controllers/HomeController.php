<?php

namespace App\Http\Controllers;

use App\Promo_code;
use App\RefDetails;
use App\User;

use Illuminate\Http\Request;
use App\Refcode;
use App\Credit;
use App\RefDiscount;
use App\Image;
use App\Video;
use Illuminate\Support\Facades\Auth;
use DB;
use File;
use SendGrid;
use SendGrid\Content;
use SendGrid\Email;
use SendGrid\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function image()
    {
        $refcode = Refcode::where('user_id', Auth::user()->id)
                ->get();

        $credit=Credit::where('user_id', Auth::user()->id)
                ->get();

        $refdetails=RefDetails::where('user_id', Auth::user()->id)
                    ->where('status','pending')
                    ->get();
        if(count($refdetails)>0) {
            $refowner = User::where('id', $refdetails[0]->owner_id)->get();
        }
        else {
            $refdetails="";
            $refowner ="";
        }
        if(!file_exists('image_data/'.Auth::user()->username)){
            File::makeDirectory('image_data/'. Auth::user()->username);
        }
        $folders=array_diff(scandir('image_data/'.Auth::user()->username), array('..', '.'));

        $refdiscount=RefDiscount::where('id',$refcode[0]->type)->get();

        if(!Empty($folders))
            $uploadFolder=$folders[2];
        else
            $uploadFolder='';

        if( session()->get('uploadFolder') =='') 
            session()->put('uploadFolder', $uploadFolder);
        
        $image=Image::where('user_name', Auth::user()->id)
            ->where('folder',session()->get('uploadFolder'))
            ->get();


        return view('image')->with('refcode',$refcode)
                            ->with('credit',$credit)
                            ->with('refdetails',$refdetails)
                            ->with('refdiscount',$refdiscount)
                            ->with('refowner',$refowner)
                            ->with('folders',$folders)
                            ->with('images',$image);
                            

    }

    public function checkuser(Request $request){
        $user=User::where('username',$request->get('username'))
                    ->get();
        if(count($user)>0){
        return 1;
        }
        else
        {return 0;}
    }

    public function checkpromocode(Request $request){
        $pro=Promo_code::where('promocode', $request->get('promocode'))
            ->where('active','active') ->get();
//    return $pro->count();
        if($pro->count()>0)
            echo $pro[0]->discount;
        else echo 0;
    }

    public function checkrefcode(Request $request){
        $pro=Refcode::where('refcode',$request->get('refcode'))->get();
//    return $pro->count();
        if($pro->count()>0)
            echo 1;
        else echo 0;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function contactsupport(Request $request){
//        echo $request->get('subject');
        $from = new Email("support@neuronsinc.com", "support@neuronsinc.com");
        $subject = $request->get('subject');
        $to = new Email("Support", $request->get('email'));
        $html="<html><body>";
        $html .= "<table width='100%'; rules='all' style='border:1px solid #3A5896;' cellpadding='10'>";
        $html .= "<tr><td colspan=2>From ".$request->get('email').",<br /><br /><br /><br />".$request->get('message')."</td></tr>";
        $html .= "</table>";
        $html .= "</body></html>";
        $content = new Content("text/html", $html);
        $mail = new Mail($from, $subject, $to, $content);

        $apiKey = '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';
        $sg = new SendGrid($apiKey);
        $response = $sg->client->mail()->send()->post($mail);
        echo "Email Send Successfully";
    }

    public function enterprisesolutions(Request $request){
        $from = new Email("sales@neuronsinc.com", "sales@neuronsinc.com");
        $subject = $request->get('subject');
        $to = new Email("Support", $request->get('email'));
        $html="<html><body>";
        $html .= "<table width='100%'; rules='all' style='border:1px solid #3A5896;' cellpadding='10'>";
        $html .= "<tr><td colspan=2>From ".$request->get('email').",<br /><br /><br /><br />".$request->get('message')."</td></tr>";
        $html .= "</table>";
        $html .= "</body></html>";
        $content = new Content("text/html", $html);
        $mail = new Mail($from, $subject, $to, $content);

        $apiKey = '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';
        $sg = new SendGrid($apiKey);
        $response = $sg->client->mail()->send()->post($mail);
        echo "Email Send Successfully";
    }

    public function refshare(Request $request){
        $from = new Email($request->get('sender'), $request->get('sender'));
        $subject = 'SHARE REFERAL CODE';
        $to = new Email("Support", $request->get('email'));
        $html="<html><body>";
        $html .= "<table width='100%'; rules='all' style='border:1px solid #3A5896;' cellpadding='10'>";
        $html .= "<tr><td colspan=2>From ".$request->get('sender').",<br /><br /><br /><br />".$request->get('message')."</td></tr>";
        $html .= "</table>";
        $html .= "</body></html>";
        $content = new Content("text/html", $html);
        $mail = new Mail($from, $subject, $to, $content);

        $apiKey = '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';
        $sg = new SendGrid($apiKey);
        $response = $sg->client->mail()->send()->post($mail);
        echo "Email Send Successfully";
    }


    public function video()
    {
        $refcode = Refcode::where('user_id', Auth::user()->id)
            ->get();

        $credit=Credit::where('user_id', Auth::user()->id)
            ->get();

        $refdetails=RefDetails::where('user_id', Auth::user()->id)
            ->where('status','pending')
            ->get();
        if(count($refdetails)>0) {
            $refowner = User::where('id', $refdetails[0]->owner_id)->get();
        }
        else {
            $refdetails="";
            $refowner ="";
        }
        if(!file_exists('video_data/'.Auth::user()->username)){
            File::makeDirectory('video_data/'. Auth::user()->username);
        }
        $folders=array_diff(scandir('video_data/'.Auth::user()->username), array('..', '.'));

        $refdiscount=RefDiscount::where('id',$refcode[0]->type)->get();

        if(!Empty($folders))
            $uploadFolder=$folders[2];
        else
            $uploadFolder='';

        if( session()->get('uploadvideoFolder') =='')
            session()->put('uploadvideoFolder', $uploadFolder);

        $video=Video::where('user_name', Auth::user()->id)
            ->where('folder',session()->get('uploadvideoFolder'))
            ->get();


        return view('video')->with('refcode',$refcode)
            ->with('credit',$credit)
            ->with('refdetails',$refdetails)
            ->with('refdiscount',$refdiscount)
            ->with('refowner',$refowner)
            ->with('folders',$folders)
            ->with('videos',$video);


    }

}
