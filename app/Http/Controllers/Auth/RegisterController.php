<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Refcode;
use App\RefDetails;
use App\Credit;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/image';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if(isset($data['refcheck'])){
            $ref='exists:refcode,refcode';
        }
        else $ref='';
        return Validator::make($data, [
            'username' => 'required|max:255|unique:users',
            'firstname'=>'required|max:255',
            'lastname'=>'required|max:255',
            'country'=>'required|max:255',
            'company'=>'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'checkbox'=>'required',
            'captcha' => 'required|captcha',
            'refcode'=>$ref,




        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
         return User::create([
            'username' => $data['username'],
            'firstname'=>$data['firstname'],
            'lastname'=>$data['lastname'],
            'country'=>$data['country'],
            'company'=>$data['company'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),

        ]);

       
    }

    protected function add(){
        return Refcode::create([
            'user_id'=>User::select('id')->orderBy('id', 'DESC')->first()->id,
            'refcode'=>uniqid(),
            'type'=>"1",
            ]);
    }

    protected function addcredit(){
        return Credit::create([
            'user_id'=>User::select('id')->orderBy('id', 'DESC')->first()->id,
            'total_credit'=>0,
        ]);
    }

    protected function addref(array $data){
        if(isset($data['refcheck'])){
            $ref=$data['refcode'];
            $checkref=Refcode::where('refcode',$ref)
                ->get();
            
            if(count($checkref)>0){
                $refdetails=new RefDetails();
                $refdetails->user_id=User::select('id')->orderBy('id', 'DESC')->first()->id;
                $refdetails->owner_id=$checkref[0]->user_id;
                $refdetails->status='pending';
                $refdetails->save();
            }
            
        }

    }

   
}
