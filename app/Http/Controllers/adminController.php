<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Video;
use App\User;
use App\Image;
use App\Credit;
use App\Promo_code;
use App\promoUser;
use App\RefDiscount;
use App\Refcode;
use App\userRef;
use App\ownerRef;
class adminController extends Controller
{
    public function login(){
        return view('admin.auth.login');
    }

    public function admin(){
        $user=User::where('is_admin',0)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();
        return view('admin.admin')->with('user',$user)->with('image',$image)
            ->with('video',$video);
        echo json_encode($video);
    }
    
    public function getuserdetails($id){
        return $user=User::where('id',$id)->get();
    }


public function index(){
    $user=User::where('is_admin',0)->get();
    $image=Image::where('download',2)->get();
    $video=Video::where('download',2)->get();

    return view('admin.user')->with('user',$user)->with('image',$image)
        ->with('video',$video);
}
    public function edit($id){
        $user=User::where('is_admin',0)->get();
        $my=User::where('id',$id)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();
        return view('admin.edituser')->with('user',$user)->with('image',$image)
            ->with('video',$video)->with('my',$my);
    }

    public function edituser(Request $request){
        $user = User::where('id',$request->get('id') )
            ->update(['firstname'=>$request->get('first_name'),'lastname'=>$request->get('last_name'),'status'=>$request->get('status'),'email' => $request->get('email')]);
        return(redirect('/admin/profile'));
    }

    public function destroy($id){
        $user = User::find($id);
        $user->delete();
        return(redirect('/admin/profile'));
    }

    public function videoupload(){
        $user=User::where('is_admin',0)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();
        return view('admin.videoupload')->with('user',$user)->with('image',$image)
            ->with('video',$video);
    }

    public function imageupload(){
        $user=User::where('is_admin',0)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();
        return view('admin.imagesupload')->with('user',$user)->with('image',$image)
            ->with('video',$video);
    }

public function managecredit(){
    $user=User::all();
    $image=Image::where('download',2)->get();
    $video=Video::where('download',2)->get();
    $usercredit = User
        ::join('credit',function($joint){ $joint->on('credit.user_id', '=', 'users.id');})
        ->get();
    return view('admin.managecredit')->with('user',$user)->with('image',$image)
        ->with('video',$video)->with('usercredit',$usercredit);
}

    public function updatecredit(Request $request){
        $user = Credit::where('id',$request->get('id') )
            ->update(['total_credit'=>$request->get('value')]);
        echo "<div class='alert alert-success'><strong>Success! Data has been updated successfully .</strong> 
                     </div>";

    }

    public function promocode(){
        $user=User::where('is_admin',0)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();
        $query = Promo_code::get();
      return view('admin.promocode')->with('user',$user)->with('image',$image)
          ->with('video',$video)->with('promocode',$query);
    }


   public function addpromo(Request $request){
           $query1 = new Promo_code();
           $query1->name=$request->get('Name');
           $query1->promocode=$request->get('code');
           $query1->discount=$request->get('discount');
           $query1->active=$request->get('active');
           $query1->save();

           return "Successfully added";

   }

    public function promousers(){
        $user=User::where('is_admin',0)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();
        $query = promoUser::get();
        return view('admin.promousers')->with('user',$user)->with('image',$image)
            ->with('video',$video)->with('promousers',$query);
    }
    
    public function referalcode(){
        $user=User::where('is_admin',0)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();
        $query = RefDiscount::get();
        return view('admin.referalcode')->with('user',$user)->with('image',$image)
            ->with('video',$video)->with('refcode',$query);
    }

    public function addref(Request $request){
        $query1 = new RefDiscount();
        $query1->Name=$request->get('Name');
        $query1->user_discount=$request->get('userdiscount');
        $query1->owner_discount=$request->get('ownerdiscount');
        $query1->active=$request->get('active');
        $query1->save();

        return "Successfully added";
    }
    public function refcode(){
        $user=User::where('is_admin',0)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();
        // $query = Refcode::get();
         $query = User
        ::join('refcode',function($joint){ $joint->on('refcode.user_id', '=', 'users.id');})
        // ::joint('refdiscount',function($joint1){ $joint1->on('refcode.type', '=', 'refdiscount.id');})
        ->get();
        $query1 = RefDiscount::where('active','active')->get();

        return view('admin.refcode')->with('user',$user)->with('image',$image)
            ->with('video',$video)->with('refcode',$query)->with('refdis',$query1);
    }

    public function userearnref(){
        $user=User::where('is_admin',0)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();
        $query = userRef::get();
        return view('admin.userearnref')->with('user',$user)->with('image',$image)
            ->with('video',$video)->with('refcode',$query);
    }

    public function ownerearnref(){
        $user=User::where('is_admin',0)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();
        $query = ownerRef::get();
        return view('admin.ownerearnref')->with('user',$user)->with('image',$image)
            ->with('video',$video)->with('refcode',$query);
    }

    public function editpromo($id){
        $user=User::where('is_admin',0)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();
        $query=Promo_code::where('id',$id)->get();
        return view('admin.editpromo')->with('user',$user)->with('image',$image)
            ->with('video',$video)->with('promo',$query);
    }
    public function geteditpromo(Request $request){
        $query=Promo_code::where('id',$request->get('id'))->get();
        return $query;
    }

    public function geteditref(Request $request){
        $ref=RefDiscount::where('id',$request->get('id'))->get();
            return $ref;
    }
    
    public function editpromocode(Request $request){
        $promo = Promo_code::where('id',$request->get('id') )
            ->update(['name'=>$request->get('Name'),'promocode'=>$request->get('code'),'discount'=>$request->get('discount'),'active'=>$request->get('active')]);
            return "successfully edited";
   // return redirect('admin/promocode');
    }

    public function editreferaldetails(Request $request){
        $ref = RefDiscount::where('id',$request->get('id') )
            ->update(['Name'=>$request->get('Name'),'user_discount'=>$request->get('userdiscount'),'owner_discount'=>$request->get('ownerdiscount'),'active'=>$request->get('active')]);
            return "successfully edited";
    }

    public function deletepromocode($id){
        Promo_code::destroy($id);
        return redirect('admin/promocode');
        
        }
    
    public function editref($id){
        $user=User::where('is_admin',0)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();
        $query=RefDiscount::where('id',$id)->get();
        return view('admin.editref')->with('user',$user)->with('image',$image)
            ->with('video',$video)->with('ref',$query); 
    }

    public function editrefdetails(Request $request){
        $user=User::where('is_admin',0)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();

        $promo = RefDiscount::where('id',$request->get('id') )
            ->update(['Name'=>$request->get('Name'),'user_discount'=>$request->get('user_discount'),'owner_discount'=>$request->get('owner_discount'),'active'=>$request->get('active')]);
        return "successfully updated";

    }

    public function deleterefdetails($id){
        RefDiscount::destroy($id);
        return redirect('admin/referalcode');
    }

    function videoheatmap(Request $request){
        $user=User::where('is_admin',0)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();
        $videoheatmap=Video::where('id',$request->get('id'))->get();
        return view('admin.videoheatmap')->with('user',$user)->with('image',$image)
            ->with('video',$video)->with('videoheatmap',$videoheatmap);
    }
    function heatmap(Request $request){
        $user=User::where('is_admin',0)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();
        $heatmap=Image::where('id',$request->get('id'))->get();
        return view('admin.heatmap')->with('user',$user)->with('image',$image)
            ->with('video',$video)->with('heatmap',$heatmap);
    }
    function fogmap(Request $request){
        $user=User::where('is_admin',0)->get();
        $image=Image::where('download',2)->get();
        $video=Video::where('download',2)->get();
        $fogmap=Image::where('id',$request->get('id'))->get();
        return view('admin.fogmap')->with('user',$user)->with('image',$image)
            ->with('video',$video)->with('fogmap',$fogmap);
    }
    public function editreftype(Request $request){
        $promo = Refcode::where('user_id',$request->get('id') )
            ->update(['type'=>$request->get('Type')]);
            return "successfully updated"; 
    }
}
