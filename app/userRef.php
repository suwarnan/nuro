<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userRef extends Model
{
    protected $table = 'user_earn_ref';
    public $timestamps = false;
    protected $fillable = [
        'user_id','earned_amount','date',
    ];
}
