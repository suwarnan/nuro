<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ownerRef extends Model
{
    protected $table = 'owner_earn_ref';
    public $timestamps = false;
    protected $fillable = [
        'user_id','earned_amount','date',
    ];
}
