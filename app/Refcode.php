<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Refcode extends Model
{
    //
    protected $table = 'refcode';
    protected $fillable = [
        'user_id','refcode','type',
    ];
}
