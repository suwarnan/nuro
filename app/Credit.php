<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    protected $table = 'credit';
    public $timestamps = false;
    protected $fillable = [
        'user_id','total_credit',
    ];
}
