<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class promoUser extends Model
{
    protected $table = 'promo_user';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'id','user_id','Date','token','promo_token',
    ];
}
