<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', 'HomeController@image');
//Route::post('/home', 'HomeController@index');
Route::get('/image','HomeController@image');
Route::post('/checkpromocode', 'HomeController@checkpromocode');
Route::post('/checkrefcode', 'HomeController@checkrefcode');
Route::post('/checkuser','HomeController@checkuser');
Route::post('/contactsupport','HomeController@contactsupport');

Route::post('/enterprisesolutions','HomeController@enterprisesolutions');
Route::post('/refshare','HomeController@refshare');
Route::post('/cfolder','ImageController@createfolder');
Route::post('/changefolder','ImageController@changefolder');
Route::post('/Deleteimage','ImageController@Deleteimage');
Route::post('/deletefolder','ImageController@deletefolder');
Route::post('/image/viewdetails','ImageController@viewdetails');
Route::post('/receive','ImageController@receive');
Route::post('/extract','ImageController@extract');
Route::post('upload', ['as' => 'upload-post', 'uses' =>'ImageController@postUpload']);
Route::post('uploadFiles', 'ImageController@uploadFiles');
Route::post('upload/delete', ['as' => 'upload-remove', 'uses' =>'ImageController@deleteUpload']);

Route::get('/video','HomeController@video');
Route::post('/cvideofolder','VideoController@createfolder');
Route::post('/uploadvideoFiles', 'VideoController@uploadFiles');
Route::post('/changevideofolder','VideoController@changefolder');
Route::post('/Deletevideo','VideoController@Deletevideo');
Route::post('/deletevideofolder','VideoController@deletefolder');
Route::post('/video/viewdetails','VideoController@viewdetails');
Route::post('/receivevideo','VideoController@receive');
Route::post('/extractvideo','VideoController@extract');
Route::post('/video/viewdetails','VideoController@viewdetails');
Route::post('/getCheckout','PaypalController@getCheckout');
Route::get('getDone', ['as'=>'getDone','uses'=>'PaypalController@getDone']);
Route::get('getCancel', ['as'=>'getCancel','uses'=>'PaypalController@getCancel']);


Route::get('admin', ['middleware' => 'admin', function () {

    return redirect('/admin/profile');
    //
}]);
Route::get('/admin/login', 'adminController@login');
    

//Route::get('/admin', function () {
//    return redirect('login');
//});
Route::group(['prefix' => '', 'middleware' => 'adminRole' ], function () {

    Route::get('/admin/profile', 'adminController@admin');
    Route::get('/admin/user/delete/{id}', 'adminController@destroy');
    Route::resource('/admin/user', 'adminController');
    Route::post('/admin/edituser', 'adminController@edituser');
    Route::get('/admin/videoupload', 'adminController@videoupload');
    Route::get('/admin/imagesupload', 'adminController@imageupload');
    Route::get('/admin/managecredit', 'adminController@managecredit');
    Route::post('/admin/updatecredit', 'adminController@updatecredit');
    Route::get('/admin/promocode', 'adminController@promocode');
    Route::get('/admin/getallpromo', 'adminController@getallpromo');
    Route::post('/admin/addpromocode', 'adminController@addpromo');
    Route::post('/admin/editpromocode', 'adminController@editpromocode');
    Route::get('admin/promo/{id}/delete', 'adminController@deletepromocode');
    Route::get('/admin/promousers', 'adminController@promousers');
    Route::get('/admin/referalcode', 'adminController@referalcode');
    Route::post('/admin/addrefcode', 'adminController@addref');
    Route::get('/admin/refcode', 'adminController@refcode');
    Route::get('/admin/userearnref', 'adminController@userearnref');
    Route::get('/admin/ownerearnref', 'adminController@ownerearnref');
    Route::get('/admin/promocode/{id}/edit', 'adminController@editpromo');
    Route::get('/admin/ref/{id}/edit', 'adminController@editref');
    Route::post('/admin/editrefdetails', 'adminController@editrefdetails');
    Route::get('admin/ref/{id}/delete', 'adminController@deleterefdetails');
    Route::post('/admin/heatmap','adminController@heatmap');
    Route::post('/admin/fogmap','adminController@fogmap');
    Route::post('admin/geteditpromo','adminController@geteditpromo');
    Route::post('admin/geteditref','adminController@geteditref');
    Route::post('/admin/editreferaldetails','adminController@editreferaldetails');
    Route::post('/admin/videoheatmap','adminController@videoheatmap');
    Route::post('/admin/editreftype','adminController@editreftype');
//     Route::get('/', function () {
//     return redirect('admin/login');
// });
});
//Route::resource('/admin/{user}/user', 'adminController@edit');
//Route::resource('photo', 'PhotoController',
//    ['except' => ['create', 'store', 'update', 'destroy']]);

// Route::get('test',function(){
//     $message  = "hello";
//     Mail::send('welcome', ['key' => 'value'], function($message)
//     {

//        $message->from('myEmail@test.com')
//            ->to('skan@irstha.com', 'John Smith')
//            ->subject('Welcome!');
//     });
// });