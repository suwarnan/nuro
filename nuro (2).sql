-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 06, 2017 at 12:53 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nuro`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `show_name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `last_name`, `show_name`, `gender`, `phone`, `image`, `password`, `email`) VALUES
(1, 'NeuronsInc', 'admin', 'admin', 'male', '9876567878', 'Penguins.jpg', '0ab1d17a968cbc08469a8f9b010525b8', 'admin@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `credit`
--

CREATE TABLE `credit` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_credit` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit`
--

INSERT INTO `credit` (`id`, `user_id`, `total_credit`) VALUES
(13, 17, '0');

-- --------------------------------------------------------

--
-- Table structure for table `ibn_table`
--

CREATE TABLE `ibn_table` (
  `id` int(11) NOT NULL,
  `itransaction_id` varchar(255) NOT NULL,
  `ipayerid` varchar(255) NOT NULL,
  `iname` varchar(255) NOT NULL,
  `iemail` varchar(255) NOT NULL,
  `ipaymentstatus` varchar(255) NOT NULL,
  `ieverything_else` text NOT NULL,
  `itransaction_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `images` varchar(255) NOT NULL,
  `folder` varchar(255) NOT NULL,
  `download` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_03_04_054837_create_refcode_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `owner_earn_ref`
--

CREATE TABLE `owner_earn_ref` (
  `user_id` int(11) NOT NULL,
  `earned_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paypal_log`
--

CREATE TABLE `paypal_log` (
  `id` int(10) NOT NULL,
  `txn_id` varchar(600) NOT NULL,
  `log` text NOT NULL,
  `posted_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promo_code`
--

CREATE TABLE `promo_code` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `promocode` varchar(100) NOT NULL,
  `discount` int(11) UNSIGNED NOT NULL,
  `active` varchar(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `promo_code`
--

INSERT INTO `promo_code` (`id`, `name`, `promocode`, `discount`, `active`) VALUES
(1, 'suwarnan', 'afgdsfsdgdzs', 3, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `promo_user`
--

CREATE TABLE `promo_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `Date` date NOT NULL,
  `token` int(11) NOT NULL,
  `promo_token` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase`
--

CREATE TABLE `purchase` (
  `id` int(11) NOT NULL,
  `invoice` varchar(300) NOT NULL,
  `name` varchar(255) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `payer_email` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `payment_status` varchar(300) NOT NULL,
  `posted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchasess`
--

CREATE TABLE `purchasess` (
  `id` int(10) NOT NULL,
  `product_name` varchar(300) NOT NULL,
  `product_quantity` varchar(300) NOT NULL,
  `product_amount` varchar(300) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `refcode`
--

CREATE TABLE `refcode` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `refcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `refcode`
--

INSERT INTO `refcode` (`user_id`, `refcode`, `type`, `created_at`, `updated_at`) VALUES
(10, '58ba6c4358c81', 1, '2017-03-04 01:56:59', '2017-03-04 01:56:59'),
(12, '58ba6ea91816b', 1, '2017-03-04 02:07:13', '2017-03-04 02:07:13'),
(17, '58bd27b995c1b', 1, '2017-03-06 03:41:21', '2017-03-06 03:41:21');

-- --------------------------------------------------------

--
-- Table structure for table `refferal_code`
--

CREATE TABLE `refferal_code` (
  `refferal_id` varchar(100) NOT NULL,
  `refferal` varchar(100) NOT NULL,
  `status` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ref_code`
--

CREATE TABLE `ref_code` (
  `user_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ref_discount`
--

CREATE TABLE `ref_discount` (
  `id` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `user_discount` int(11) NOT NULL,
  `owner_discount` int(11) NOT NULL,
  `active` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_discount`
--

INSERT INTO `ref_discount` (`id`, `Name`, `user_discount`, `owner_discount`, `active`) VALUES
(1, 'suwanan', 4, 3, 'active'),
(2, 'Test', 7, 4, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Block','Unblock') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Block',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `firstname`, `lastname`, `country`, `company`, `status`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(10, 'NeuronsInc', 'tester', 'tester1', 'Australia', 'abcd', 'Block', 'skan@irstha.com', '$2y$10$IcOD.P5kavOcEVp81LPv7O3CfKbFnm7YvQWIqWlrSawKHI1wtmD7a', '8z9ME4pn42eG3sytE7z9XCfwxQvaPnRNsoUltTuPBEzH274U8YKtNLqVcA6P', '2017-03-04 01:56:59', '2017-03-04 01:56:59'),
(11, 'NeuronsIncs', 'tester', 'dfsg', 'Aruba', 'zxczxc', 'Block', 'skan1@irstha.com', '$2y$10$1sYkE2mLgvPLmb0uTkKJrOulGzghMG31L5BDKYYWW7XetAv2VHDT.', 'U9MNTUqCz16gP0vHa0Bk7g2jowa2UbadOokwmtWDbQ58PCkqBGkFzEwl9bcd', '2017-03-04 01:58:53', '2017-03-04 01:58:53'),
(12, 'test', 'tester', 'xcbfx', 'Bahamas', 'abcd', 'Block', 'ra@gmail.com', '$2y$10$9TB.ByZJtCTfNzoJy9LpueobxXTQilP8wea2yBqgOKFzoq/D7tecm', 'VJTaF7j36WywwmqKmlfI66YCaaz84mimzMzd65snPxTGNm5OhnSCwQ7QkYYc', '2017-03-04 02:07:13', '2017-03-04 02:07:13'),
(17, 'test1', 'reg', 'dfsg', 'Anguilla', 'dsfg', 'Block', 'tha@gmail.com', '$2y$10$A/h/tlGkrCHH7gkw22yFfuQe.OgH492MDFbkqsf/OKXAGCi.VdQfe', 'T6osDQwnXguAw1LjhkUdnLlzqz0NnpQNw1XQtnX8NEBSAbq1mFiSPuRldBrJ', '2017-03-06 03:41:21', '2017-03-06 03:41:21');

-- --------------------------------------------------------

--
-- Table structure for table `user_earn_ref`
--

CREATE TABLE `user_earn_ref` (
  `user_id` int(11) NOT NULL,
  `earned_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `video_name` varchar(255) NOT NULL,
  `uploaded` datetime NOT NULL,
  `folder` varchar(255) NOT NULL,
  `download` int(11) NOT NULL,
  `bitrate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit`
--
ALTER TABLE `credit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ibn_table`
--
ALTER TABLE `ibn_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner_earn_ref`
--
ALTER TABLE `owner_earn_ref`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `paypal_log`
--
ALTER TABLE `paypal_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promo_code`
--
ALTER TABLE `promo_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promo_user`
--
ALTER TABLE `promo_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchasess`
--
ALTER TABLE `purchasess`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refcode`
--
ALTER TABLE `refcode`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `refferal_code`
--
ALTER TABLE `refferal_code`
  ADD PRIMARY KEY (`refferal_id`);

--
-- Indexes for table `ref_code`
--
ALTER TABLE `ref_code`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `ref_discount`
--
ALTER TABLE `ref_discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_earn_ref`
--
ALTER TABLE `user_earn_ref`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `credit`
--
ALTER TABLE `credit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `ibn_table`
--
ALTER TABLE `ibn_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `paypal_log`
--
ALTER TABLE `paypal_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `promo_code`
--
ALTER TABLE `promo_code`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `promo_user`
--
ALTER TABLE `promo_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `purchase`
--
ALTER TABLE `purchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchasess`
--
ALTER TABLE `purchasess`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ref_discount`
--
ALTER TABLE `ref_discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `refcode`
--
ALTER TABLE `refcode`
  ADD CONSTRAINT `refcode_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
