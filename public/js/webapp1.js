$(document).ready(function(){

  var table_companies = $('#table_companies').dataTable({
    "ajax": '{{url("/admin/promo")}}?job=get_companies',
    "columns": [
      { "data": "Name",   "sClass": "Name" },
      { "data": "code",        "sClass": "Name" },
      { "data": "discount",      "sClass": "integer" },
      { "data": "active",     "sClass": "active" },
      { "data": "functions",      "sClass": "functions" }
    ],
    "aoColumnDefs": [
      { "bSortable": false, "aTargets": [-1] }
    ],
    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
    "oLanguage": {
      "oPaginate": {
        "sFirst":       " ",
        "sPrevious":    " ",
        "sNext":        " ",
        "sLast":        " ",
      },
      "sLengthMenu":    "Records per page: _MENU_",
      "sInfo":          "Total of _TOTAL_ records (showing _START_ to _END_)",
      "sInfoFiltered":  "(filtered from _MAX_ total records)"
    }
  });

    var table_promocode = $('#table_promocode').dataTable({
    "ajax": '{{url("/admin/promo")}}?job=get_promo',
    "columns": [
      { "data": "id",   "sClass": "integer" },
      { "data": "username",   "sClass": "Name" },
      { "data": "date",        "sClass": "integer" },
      { "data": "token",    "sClass": "integer" },
      { "data": "discount",      "sClass": "integer" }
    ],
    "aoColumnDefs": [
      { "bSortable": false, "aTargets": [-1] }
    ],
    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
    "oLanguage": {
      "oPaginate": {
        "sFirst":       " ",
        "sPrevious":    " ",
        "sNext":        " ",
        "sLast":        " ",
      },
      "sLengthMenu":    "Records per page: _MENU_",
      "sInfo":          "Total of _TOTAL_ records (showing _START_ to _END_)",
      "sInfoFiltered":  "(filtered from _MAX_ total records)"
    }
  });

    
//   $.validator.addMethod("greaterThan",

//         function (value, element, param) {
//         var $min = $(param);
//         if (this.settings.onfocusout) {
//         $min.off(".validate-greaterThan").on("blur.validate-greaterThan", function () {
//           $(element).valid();
//         });
//       }
//       return parseInt(value) > parseInt($min.val());
//     }, "First amount must be greater than last amount");

// $('#form_company').validate({
//   rules: {
//     lamount: {
//       greaterThan: '#famount'
//     }
//   }
// });
  
  // On page load: form validation
  // jQuery.validator.setDefaults({
  //   success: 'valid',
  //   rules: {
  //     fiscal_year: {
  //       required: true,
  //       min:      2000,
  //       max:      2025
  //     }
  //   },
  //   errorPlacement: function(error, element){
  //     error.insertBefore(element);
  //   },
  //   highlight: function(element){
  //     $(element).parent('.field_container').removeClass('valid').addClass('error');
  //   },
  //   unhighlight: function(element){
  //     $(element).parent('.field_container').addClass('valid').removeClass('error');
  //   }
  // });
  var form_company = $('#form_company');
  form_company.validate();

  // Show message
  function show_message(message_text, message_type){
    $('#message').html('<p>' + message_text + '</p>').attr('class', message_type);
    $('#message_container').show();
    if (typeof timeout_message !== 'undefined'){
      window.clearTimeout(timeout_message);
    }
    timeout_message = setTimeout(function(){
      hide_message();
    }, 8000);
  }
  // Hide message
  function hide_message(){
    $('#message').html('').attr('class', '');
    $('#message_container').hide();
  }

  // Show loading message
  function show_loading_message(){
    $('#loading_container').show();
  }
  // Hide loading message
  function hide_loading_message(){
    $('#loading_container').hide();
  }

  // Show lightbox
  function show_lightbox(){
    $('.lightbox_bg').show();
    $('.lightbox_container').show();
  }
  // Hide lightbox
  function hide_lightbox(){
    $('.lightbox_bg').hide();
    $('.lightbox_container').hide();
  }
  // Lightbox background
  $(document).on('click', '.lightbox_bg', function(){
    hide_lightbox();
  });
  // Lightbox close button
  $(document).on('click', '.lightbox_close', function(){
    hide_lightbox();
  });
  // Escape keyboard key
  $(document).keyup(function(e){
    if (e.keyCode == 27){
      hide_lightbox();
    }
  });
  
  // Hide iPad keyboard
  function hide_ipad_keyboard(){
    document.activeElement.blur();
    $('input').blur();
  }

  // Add company button
  $(document).on('click', '#add_company', function(e){
    e.preventDefault();
    $('.lightbox_content h2').text('ADD NEW PROMOCODE');
    $('#form_company button').text('Add NEW');
    $('#form_company').attr('class', 'form add');
    $('#form_company').attr('data-id', '');
    $('#form_company .field_container label.error').hide();
    $('#form_company .field_container').removeClass('valid').removeClass('error');
    $('#form_company #Name').val('');
    $('#form_company #code').val('');
    $('#form_company #discount').val('');
    $('#form_company #active').val('');
    show_lightbox();


  

  });

  // Add company submit form
  $(document).on('submit', '#form_company.add', function(e){
    e.preventDefault();
    // Validate form
    if (form_company.valid() == true){
      // Send company information to database
      var crsfToken = $( "#_token" ).val();
      hide_ipad_keyboard();
      hide_lightbox();
      show_loading_message();
      var form_data = $('#form_company').serialize();
      var request   = $.ajax({
        url:          '{{url("/admin/promo")}}?job=add_company',
        headers: {
          "X-CSRF-TOKEN": crsfToken
        },
        cache:        false,
        data:         form_data,
        dataType:     'json',
        contentType:  'application/json; charset=utf-8',
        type:         'get'
      });
      request.done(function(output){
        if (output.result == 'success'){
          // Reload datable
          table_companies.api().ajax.reload(function(){
            hide_loading_message();
            var company_name = $('#Name').val();
            show_message("Company '" + company_name + "' added successfully.", 'success');
          }, true);
        } else {
          hide_loading_message();
          show_message('Add request failed', 'error');
        }
      });
      request.fail(function(jqXHR, textStatus){
        hide_loading_message();
        show_message('Add request failed: ' + textStatus, 'error');
      });
    }
  });

  // Edit company button
  $(document).on('click', '.function_edit a', function(e){
    e.preventDefault();
    // Get company information from database
    show_loading_message();
    var crsfToken = $( "#_token" ).val();
    var id      = $(this).data('id');
    var request = $.ajax({
      url:          '{{url("/admin/promo")}}?job=get_company',
      cache:        false,
      data:         'id=' + id,
      dataType:     'json',
      contentType:  'application/json; charset=utf-8',
      type:         'get',
      headers: {
        "X-CSRF-TOKEN": crsfToken
      }
    });
    request.done(function(output){
      if (output.result == 'success'){
        $('.lightbox_content h2').text('Edit Promocode');
        $('#form_company button').text('Edit');
        $('#form_company').attr('class', 'form edit');
        $('#form_company').attr('data-id', id);
        $('#form_company .field_container label.error').hide();
        $('#form_company .field_container').removeClass('valid').removeClass('error');
        $('#form_company #Name').val(output.data[0].Name);
        
        $('#form_company #code').val(output.data[0].code);
        $('#form_company #discount').val(output.data[0].discount);
        $('#form_company #active').val(output.data[0].active);
        
        hide_loading_message();
        show_lightbox();
      } else {
        hide_loading_message();
        show_message('Information request failed', 'error');
      }
    });
    request.fail(function(jqXHR, textStatus){
      hide_loading_message();
      show_message('Information request failed: ' + textStatus, 'error');
    });
  });
  
  // Edit company submit form
  $(document).on('submit', '#form_company.edit', function(e){
    e.preventDefault();
    // Validate form
    if (form_company.valid() == true){
      // Send company information to database
      hide_ipad_keyboard();
      hide_lightbox();
      show_loading_message();
      var id        = $('#form_company').attr('data-id');
      var form_data = $('#form_company').serialize();
      var crsfToken = $( "#_token" ).val();
      var request   = $.ajax({
        url:          '{{url("/admin/promo")}}?job=edit_company&id=' + id,
        cache:        false,
        data:         form_data,
        dataType:     'json',
        contentType:  'application/json; charset=utf-8',
        type:         'get',
        headers: {
          "X-CSRF-TOKEN": crsfToken
        }
      });
      request.done(function(output){
        if (output.result == 'success'){
          // Reload datable
          table_companies.api().ajax.reload(function(){
            hide_loading_message();
            var company_name = $('#Name').val();
            show_message( company_name + "' edited successfully.", 'success');
          }, true);
        } else {
          hide_loading_message();
          show_message('Edit request failed', 'error');
        }
      });
      request.fail(function(jqXHR, textStatus){
        hide_loading_message();
        show_message('Edit request failed: ' + textStatus, 'error');
      });
    }
  });
  
  // Delete company
  $(document).on('click', '.function_delete a', function(e){
    e.preventDefault();
    var company_name = $(this).data('name');
    if (confirm("Are you sure you want to delete '" + company_name + "'?")){
      show_loading_message();
      var id      = $(this).data('id');
      var crsfToken = $( "#_token" ).val();
      var request = $.ajax({
        url:          '{{url("/admin/promo")}}?job=delete_company&id=' + id,
        cache:        false,
        dataType:     'json',
        contentType:  'application/json; charset=utf-8',
        type:         'get',
        headers: {
          "X-CSRF-TOKEN": crsfToken
        }
      });
      request.done(function(output){
        if (output.result == 'success'){
          // Reload datable
          table_companies.api().ajax.reload(function(){
            hide_loading_message();
            show_message("Company '" + company_name + "' deleted successfully.", 'success');
          }, true);
        } else {
          hide_loading_message();
          show_message('Delete request failed', 'error');
        }
      });
      request.fail(function(jqXHR, textStatus){
        hide_loading_message();
        show_message('Delete request failed: ' + textStatus, 'error');
      });
    }
  });



});